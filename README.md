KETJU edition of GADGET-4
=========================

This is a modified version of the GADGET-4 code that includes the KETJU black hole
dynamics features developed by the Theoretical Extragalactic Research Group at the University of Helsinki.
See [documentation/12_ketju.md](documentation/12_ketju.md) and the [code paper] for more details on the added functionality.
See the [code repository] for the up-to-date version of the code.

Check out also the [KETJU project website] for an overview of what has been done using KETJU.

When publishing work using this code, please cite the [code paper], the [integrator code paper],
and the original [GADGET-4 code paper].

If you encounter issues when using the code, you can report them using the [issue tracker].

[code paper]: https://ui.adsabs.harvard.edu/abs/2023arXiv230604963M
[integrator code paper]: https://ui.adsabs.harvard.edu/abs/2020MNRAS.492.4131R
[GADGET-4 code paper]: https://ui.adsabs.harvard.edu/abs/2021MNRAS.506.2871S 
[KETJU project website]: https://www.mv.helsinki.fi/home/phjohans/ketju
[code repository]: https://bitbucket.org/helsinkiextragalacticgroup/gadget4-ketju
[issue tracker]: https://bitbucket.org/helsinkiextragalacticgroup/gadget4-ketju/issues 


Building and running
--------------------

For building the code, you will need to first build the integrator library from the
[Ketju integrator repository](https://bitbucket.org/helsinkiextragalacticgroup/ketju-integrator).

By default, the build system assumes that the integrator library directory is
found on the relative path `../ketju-integrator`. 
If you have placed the integrator directory somewhere else, you need to  set the 
`KETJU_INTEGRATOR_DIR` variable, either during the `make` call 
by adding the argument `-DKETJU_INTEGRATOR_DIR=path/to/ketju-integrator`
or by setting the variable in e.g. `Makefile.systype`.

Otherwise the code is built using the same process as standard GADGET-4,
with the `KETJU` option added to `Config.sh`.
See [documentation/02_running.md](documentation/02_running.md) for further details.


The new configuration options and parameterfile options are explained
in the standard documentation files 
[documentation/04_config-options.md](documentation/04_config-options.md) 
and [documentation/05_parameterfile.md](documentation/05_parameterfile.md).


An example run using Ketju is included in `examples/KetjuBHs/`.
You can build it simply by running  
`make DIR=examples/KetjuBHs/`  
See the [example's README](examples/KetjuBHs/README.md) for further details on running the example.


On compatibility with standard GADGET-4
---------------------------------------

Apart from the additions needed for KETJU, the changes in this code compared to
the original GADGET-4 are very limited.
When compiled without the `KETJU` config option we expect that the current code
works exactly like the original code, although we do not explicitly test for this.
In case you encounter issues that are also present without the `KETJU` option,
you should therefore first check if the issue is present also in the original GADGET-4
code (see the original README below).


### For maintainers: merging upstream changes
This code should be kept up to date with the fixes done to the original GADGET-4
version by periodically merging in the changes done to the 
[original code's repository](https://gitlab.mpcdf.mpg.de/vrs/gadget4). 
This is easy to do with `git`.

The simplest way is to simply run

```
git pull https://gitlab.mpcdf.mpg.de/vrs/gadget4.git
```

to pull and merge the changes from the upstream repository.

Setting up an additional remote allows for some more control during the merging
process, for example looking at the diff before doing the merge.
Add the `upstream` remote for your local clone of this repository by running:  

```
git remote add upstream https://gitlab.mpcdf.mpg.de/vrs/gadget4.git
```  

You can then fetch in the new commits from the upstream repository with  

```
git fetch upstream  
```  

and merge them with

```
git merge upstream/master  
```




---

Original GADGET-4 README follows:

---


GADGET-4
========

![](documentation/img/top.jpg)

GADGET-4 is a massively parallel code for N-body/hydrodynamical
cosmological simulations. It is a flexible code that can be applied to
a variety of different types of simulations, offering a number of
sophisticated simulation algorithms.  An account of the numerical
algorithms employed by the code is given in the original code paper,
subsequent publications, and this documentation.

GADGET-4 was written mainly by
[Volker Springel](mailto:vspringel@mpa-garching.mpg.de), with
important contributions and suggestions being made by numerous people,
including [Ruediger Pakmor](mailto:rpakmor@mpa-garching.mpg.de),
[Oliver Zier](mailto:ozier@mpa-garching.mpg.de), and
[Martin Reinecke](mailto:martin@mpa-garching.mpg.de).


Documentation
=============

For documentation of the code as well as the code paper, please refer
to the [code's web-site](https://wwwmpa.mpa-garching.mpg.de/gadget4).


