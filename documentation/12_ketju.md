Ketju: Regularized integrator subsystems around BHs
============================================

The Ketju module adds a high-accuracy regularized integrator to GADGET-4 to allow
capturing the dynamics near supermassive black holes (SMBHs) below the resolution limits set by the softened gravity.
In particular, Ketju allows modeling the BHs as point particles in all interactions
involving BHs and stellar particles.
It also includes post-Newtonian corrections in the interactions between BHs,
which capture relativistic effects such orbital precession and energy loss due to
GW emission.
See also the [code paper](*TODO code paper link when done*) and references therein
for more details and tests of the code.

The integrator itself is provided as a library in a [separate repository](https://bitbucket.org/helsinkiextragalacticgroup/ketju-integrator),
and to build the code with Ketju you need to build the integrator first.
The main new configuration flag for enabling Ketju is simply `KETJU` 
but see also [04_config-options.md](04_config-options.md) for options required
together with it as well as some additional options.
The various new parameterfile options are explained in [05_parameterfile.md](05_parameterfile.md).

Some resolution and accuracy recommendations
-------------------------------------------

The typical simulations we expect Ketju to be used for are simulations of galaxy mergers,
where the mass resolution used for stellar particles is well above physical stellar masses,
perhaps typically around 10^5 Msol.
In this case the accuracy and validity of the SMBH dynamics is affected by resolution
related numerical effects, such as individual stellar interactions significantly
perturbing low-mass SMBH binaries, SMBHs undergoing large-scale random walks in the stellar background,
and interactions with tight SMBH binaries being too scarce to resolve the correct binary hardening
behavior.
To avoid most of these effects, systems which follow typical SMBH-galaxy scaling relations should
target stellar resolutions where the BHs are at least several thousand times more massive than
the stellar particles.
But this is only a general recommendation, and we suggest studying the resolution related behavior of
the system under investigation to understand how well the results have converged.
In general full convergence in the SMBH dynamics appears to be impossible to achieve due to the
chaotic nature of N-body systems.

In addition to the mass resolution, the integration tolerance parameters are also important
for good results.
In addition to setting the `KetjuIntegrationRelativeTolerance` parameter to a suitable value,
we also recommend using a relatively tight tolerance for `ErrTolIntAccuracy`,
as it controls e.g. the accuracy of the large-scale orbits of SMBHs in galaxies
(we have typically used values around 1e-3 for merger runs).
The force error tolerance parameter `ErrTolForceAcc` can usually be left to its default
recommended value of 0.005, as it only affects the long-range perturbations of the
dynamics within the regularized regions.

Softening lengths of stellar particles need to also be chosen correctly for the simulation
to perform and behave well.
This is mainly due to the requirement that the regularized regions have a size of at least the
stellar softening kernel.
Using small softening values can therefore help with the regularized integration performance,
but this can also have a negative impact on the performance and accuracy of the surrounding
GADGET-integrated system.
A good starting point is to set the softening so that there are a few hundred to
a thousand stellar particles within the regularized regions when using a nearly minimum
region size.

Spin field in snapshots
-----------------------
An additional `Spins` field is added to snapshot files for BH particles
when the code is compiled with the `KETJU` config option enabled.
By default this means `PartType5`,
but this is configurable with the `KETJU_BH_PARTICLE_TYPE_FLAGS` config option.

The field contains the spin angular momentum vector of the BH, i.e. `J = G M^2 X / c`,
where `X` is the dimensionless  spin parameter vector with `|X| < 1`.
This field can also optionally be included in the initial condition file to specify the initial
spin of the BH particles, otherwise the BH spins are set to 0.



Output format
-------------

The Ketju module outputs BH data into the file `ketju_bhs.hdf5` at time intervals
specified by the `KetjuOutputTimeInterval` parameter.
The main BH data in this file can be conveniently read using the included `ketju_utils`
Python package, which also includes tools for finding bound binaries and calculating their
PN-corrected orbital parameters.

When restarting the run with a restartflag value of 1 or 2, the code will attempt
to append the new data to the existing output file if possible.
This can lead to some duplicated data points if e.g.\ the code has crashed for
some reason previously.
The input routine in `ketju_utils` handles removing such duplicated points.

The full output file contains some additional data that may be useful in some cases.
The file structure is as follows:

Group `/BHs`:
Contains arrays of structured data in datasets named based on the BH particle ID.
Each entry in the arrays contains the following fields:  
    `gadget_position` The coordinates of the BH in the internal GADGET coordinate system (potentially comoving)  
    `gadget_velocity` The internal velocity GADGET value  
    `physical_position` Physical coordinates of the BH,
                        possibly wrapped in periodic boxes if the region is near the box edge  
    `physical_velocity` Physical velocity of the BH  
    `spin` Spin angular momentum of the BH  
    `mass` mass of the BH  
    `timestep_index` index into the `/timesteps` array for the current output point  
    `num_particles_in_region` number of total particles in the regularized region containing this BH  
    `num_BHs_in_region` number of BHs in the regularized region containing this BH 


Dataset `/timesteps`:
Contains an array of structured data with the following fields giving the output data times:  
    `physical_time` Physical time, either since the start of the simulation in non-comoving or since a=0 in comoving runs.  
    `scale_factor` The scale factor a in the simulation, always 1 in non-comoving runs.

Dataset `/mergers`:
Array of structured data containing information on any BH mergers that have occurred during the simulation.
Each entry contains the following fields ("first" and "second" simply mean the two BHs in the merging binary in the internal storage order used by the code):  
    `ID1` ID of the first BH in the merger  
    `ID2` ID of the second BH in the merger  
    `ID_remnant` ID of the merger remnant   
    `m1` Mass of the first BH in the merger  
    `m2` Mass of the second BH in the merger  
    `m_remnant` Mass of the remnant  
    `chi1` Dimensionless spin parameter of the first BH   
    `chi2` Dimensionless spin parameter of the second BH   
    `chi_remnant` Dimensionless spin parameter of the remnant  
    `kick_velocity` Remnant recoil kick velocity, applied if the option `KetjuBHMergerKicks` is set to 1  
    `merger_physical_time` Physical time in the simulation when the merger occurred   
    `merger_redshift` Redshift in the simulation when the merger occurred   

Attribute: `/units`
Unit system used in the simulation.
Contains the fields:  
    `unit_time_in_s`  
    `unit_length_in_cm`  
    `unit_mass_in_g`  
    `unit_velocity_in_cm_per_s`  
    `G_cgs`  
    `c_cgs`  
    `timebase_interval`

Attribute: `/integrator options`
Ketju regularized integration options specified in the parameter file,
with slightly different names due to legacy reasons.
Contains the fields:  
    `minimum_bh_mass`  
    `region_physical_radius`  
    `output_time_interval`  
    `PN_terms`  
    `enable_bh_merger_kicks`  
    `use_star_star_softening`  
    `expand_tight_binaries_period_factor`  
    `integration_relative_tolerance`  
    `output_time_relative_tolerance`  
    `minimum_particles_per_task`  
    `use_divide_and_conquer_mst`  
    `max_tree_distance`  
    `steps_between_mst_reconstruction`  

Attribute: `/cosmology`  
A structured value containing the values of the cosmological parameters used in the simulation,
as specified in the parameterfile.
Contains the fields:  
    `HubbleParam`   
    `Omega0`   
    `OmegaLambda`  
    `OmegaBaryon`  
    `ComovingIntegrationOn`  
    `Periodic`  
    `Boxsize`

