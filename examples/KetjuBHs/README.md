Simple Ketju example
====================

Building
--------

To build the executable for running this executable run `make` in the code top level directory,
setting `DIR` to this directory:  
`make DIR=examples/KetjuBHs/`  

The code configuration provided in the `Config.sh` file is fairly minimal,
and not necessarily the optimal for performance.

Running
-------

To run this example, first download the IC file using  
```
    wget https://bitbucket.org/helsinkiextragalacticgroup/gadget4-ketju/downloads/KetjuBHs_example_ics.hdf5
```  
or by downloading the file from that address using some other means.

The ICs contain a low resolution galaxy model including three SMBHs, with two of them in a binary.
The system is small enough to run even on a single core, and mainly serves to demonstrate the 
BH output.

Then run the code as usual, e.g. with  
`mpirun -np 2 ./Gadget4 paramfile`  
The simulation should take about ten minutes to complete on a typical desktop machine. 

Analysis
--------
To analyse the simulation results for BH motion, it's easiest to use the
It uses of the `ketju-utils` Python package,
which can be found in the [`ketju-utils` repository](https://bitbucket.org/helsinkiextragalacticgroup/ketju-utils).
It includes a simple example analysis script`examples/analysis.py`,
which can be used to directly produce some simple plots of the BHs.

After running the simulation and installing `ketju-utils`, run the analysis script with the command  
`python3 path/to/ketju-utils/examples/analysis.py output/ketju_bhs.hdf5`  
to plot the BH trajectories and the orbital parameters of BH binaries.
The plots should show a tight BH binary with decreasing semimajor axis,
with a third BH orbiting it on a wide orbit.

Due to the low resolution of the sample ICs the results show sudden jumps in the
orbital parameters of the binaries due to interactions with stars.
Note also that the script plots the orbital parameters for all pairs of BHs with
pairwise negative total energy, so distant pairs of BHs may be shown as binaries 
with physically meaningless parameters for short periods of time.
