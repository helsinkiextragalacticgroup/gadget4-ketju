// Ketju module for GADGET-4.
// Copyright (C) 2022 Matias Mannerkoski and contributors.
// Licensed under the GPLv3 license. See LICENSE for details.

#ifndef KETJU_INTERFACE_H
#define KETJU_INTERFACE_H

#include "gadgetconfig.h"

// Check some incompatible options
#ifdef FORCE_EQUAL_TIMESTEPS
#error "KETJU is not compatible with FORCE_EQUAL_TIMESTEPS"
#endif

#ifdef LEAN
#error "KETJU is not compatible with LEAN"
#endif

#ifdef LIGHTCONE
#error "KETJU is not compatible with LIGHTCONE"
#endif

#ifdef OUTPUT_NON_SYNCHRONIZED_ALLOWED
#error "KETJU is not compatible with OUTPUT_NON_SYNCHRONIZED_ALLOWED"
#endif

#if !defined(DOUBLEPRECISION) || DOUBLEPRECISION != 1
#error "KETJU requires DOUBLEPRECISION=1"
#endif

#if NTYPES != 6 && (!defined(KETJU_STAR_PARTICLE_TYPE_FLAGS) || !defined(KETJU_BH_PARTICLE_TYPE_FLAGS))
#error "KETJU requires either NTYPES = 6 or setting KETJU_STAR_PARTICLE_TYPE_FLAGS and KETJU_BH_PARTICLE_TYPE_FLAGS"
#endif

#ifndef SELFGRAVITY
#error "KETJU requires SELFGRAVITY"
#endif

// Default options
#ifndef KETJU_STAR_PARTICLE_TYPE_FLAGS
// NTYPES == 6 enforced above
#define KETJU_STAR_PARTICLE_TYPE_FLAGS (1 << 4)
#endif

#ifndef KETJU_BH_PARTICLE_TYPE_FLAGS
#define KETJU_BH_PARTICLE_TYPE_FLAGS (1 << 5)
#endif

#ifndef KETJU_INTEGRATOR_MAX_STEP_COUNT
#define KETJU_INTEGRATOR_MAX_STEP_COUNT 5000000
#endif

#ifndef KETJU_TIMESTEP_LIMITING_RADIUS_FACTOR
#define KETJU_TIMESTEP_LIMITING_RADIUS_FACTOR 100.
#endif

// Forward declare some types needed for arguments
class simparticles;
class IO_Def;
class restart;

template <typename T>
class domain;

namespace ketju
{
// configurable options, set from the config file
struct Options
{
  // Main behavior options

  double minimum_bh_mass;
  // Minimum mass of a BH particle that gets a ketju region, smaller ones
  // are treated like stellar particles by the subsystem integrator.

  double region_physical_radius;
  double output_time_interval;

  char PN_terms[20];
  int enable_bh_merger_kicks;
  int use_star_star_softening;
  double expand_tight_binaries_period_factor;

  // accuracy parameters

  double integration_relative_tolerance;
  double output_time_relative_tolerance;

  // performance tuning

  double minimum_particles_per_task;
  // A tuning parameter for parallelization of the integrator to avoid using
  // too many tasks.

  int use_divide_and_conquer_mst;
  int max_tree_distance;
  int steps_between_mst_reconstruction;
};

// Data that persists after restarts in All and is the same for all tasks
struct Data
{
  Options options;
  long long final_written_Ti;
  int output_timebin;
  int next_tstep_index;
};

// Interface to the other code through free functions,
// so any data storage classes can be hidden in the implementation file.
// These modify the particle data through the passes pointer.
void find_regions(const simparticles &Sp);
void run_integration(simparticles &Sp);
void set_final_velocities(simparticles &Sp);
void finish_step(simparticles &Sp);
bool is_particle_timestep_limited(int index);
int set_limited_timebins(simparticles &Sp, int min_timebin = 0, int push_down_flag = 0);

}  // namespace ketju

#endif  // ifndef KETJU_INTERFACE_H
