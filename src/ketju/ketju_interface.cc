// Ketju module for GADGET-4.
// Copyright (C) 2022 Matias Mannerkoski and contributors.
// Licensed under the GPLv3 license. See LICENSE for details.

#include "gadgetconfig.h"

#include <hdf5.h>
#include <mpi.h>

// Unlike the rest of the code, we're using STL containers here.
// This bypasses the global memory allocation pool, but the only real problem
// that could cause is crashing with a not-so-nice error message when the memory
// is full, and some undercounting of total memory usage.
// It might be possible to implement a custom allocator that uses the global
// allocation pool if this choice causes issues, but so far such issues have not appeared.
#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstddef>
#include <cstdio>
#include <cstring>
#include <iterator>
#include <limits>
#include <memory>
#include <numeric>
#include <queue>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "../data/allvars.h"
#include "../data/simparticles.h"
#include "../domain/domain.h"
#include "../io/restart.h"
#include "../logs/logs.h"
#include "../mpi_utils/mpi_utils.h"
#include "../mpi_utils/shared_mem_handler.h"
#include "../time_integration/driftfac.h"
#include "ketju.h"
#include "ketju_integrator/ketju_integrator.h"

// This file consists mostly of class definitions and their methods that are only
// called through the few interface functions declared in the ketju.h header.
// There's quite a bit of code, and consequently this file is somewhat long,
// but breaking it into separate files is somewhat inconvenient due to the way
// the build system is implemented.
// The classes and their methods are organized into clear blocks,
// containing also any helper functions needed by the methods,
// with the interface functions at the end,
// so this file should be clear enough as is.

// Anonymous namespace for implementation details (internal linkage)
namespace
{
using namespace ketju;

constexpr unsigned int region_member_flags = (KETJU_STAR_PARTICLE_TYPE_FLAGS) | (KETJU_BH_PARTICLE_TYPE_FLAGS);
constexpr unsigned int star_particle_flags = (KETJU_STAR_PARTICLE_TYPE_FLAGS);
constexpr unsigned int bh_particle_flags   = (KETJU_BH_PARTICLE_TYPE_FLAGS);
constexpr unsigned int all_particles_flags = ~0;

static_assert((star_particle_flags & bh_particle_flags) == 0, "Cannot have overlapping star and BH particle types");
static_assert((region_member_flags & 1) == 0, "Cannot have gas as a KETJU region member type (star or BH)");
static_assert(star_particle_flags, "Cannot have empty KETJU_STAR_PARTICLE_TYPE_FLAGS");
static_assert(bh_particle_flags, "Cannot have empty KETJU_BH_PARTICLE_TYPE_FLAGS");

bool check_type_flag(int type, unsigned int flags) { return (1 << type) & flags; }
bool is_bh_type(int type) { return check_type_flag(type, bh_particle_flags); }
bool is_star_type(int type) { return check_type_flag(type, star_particle_flags); }

int get_star_softening_class()
{
  static int soft_class = []() {
    for(int n = 1; n < NTYPES; ++n)
      {
        if(is_star_type(n))
          return All.SofteningClassOfPartType[n];
      }
    Terminate("This should be impossible.");
    return -1;
  }();
  return soft_class;
}

template <typename... Ts>
void ketju_printf(const char *msg_template, const Ts &...args)
{
  // We can't always print from task 0, so the messages might get out of order.
  // Print the sync-point to make tracking this easier.
  std::printf("Ketju (Sync-Point %d): ", All.NumCurrentTiStep);
  std::printf(msg_template, args...);
  std::fflush(stdout);
}

template <typename... Ts>
void ketju_debug_printf(const char *msg_template, const Ts &...args)
{
#ifdef KETJU_DEBUG
  std::printf("Ketju DEBUG (Sync-Point %d): ", All.NumCurrentTiStep);
  std::printf(msg_template, args...);
  std::fflush(stdout);
#endif
}

double vector_norm(const double vec[3]) { return std::sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]); }

/////////////////

// Particle data communicated between tasks, values are in the global gadget frame.
struct mpi_particle
{
  MyIDType ID;
  int Type;
  // where in the particle data array (simparticles::P) and on which task the particle is located
  int Task;
  int Index;

  double Mass;
  MyIntPosType IntPos[3];
  double Vel[3];
  double Spin[3];
  double potential_energy_correction;  // only needed with EVALPOTENTIAL, but simplest to include always

  static MPI_Datatype get_mpi_datatype();
  static mpi_particle from_gadget_particle_index(const simparticles &Sp, int task, int index);
};

MPI_Datatype mpi_particle::get_mpi_datatype()
{
  // construct the datatype on the first call
  static MPI_Datatype dtype = []() {
    MPI_Datatype dtype;
    const int n_items         = 9;
    const int block_lengths[] = {1, 1, 1, 1, 1, 3, 3, 3, 1};

    static_assert(std::is_same<MyIDType, unsigned int>::value || std::is_same<MyIDType, unsigned long long>::value);
    MPI_Datatype ID_type = std::is_same<MyIDType, unsigned int>::value ? MPI_UNSIGNED : MPI_UNSIGNED_LONG_LONG;

    MPI_Datatype types[] = {ID_type, MPI_INT, MPI_INT, MPI_INT, MPI_DOUBLE, MPI_MyIntPosType, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE};
    MPI_Aint offsets[]   = {
        offsetof(mpi_particle, ID),    offsetof(mpi_particle, Type), offsetof(mpi_particle, Task),
        offsetof(mpi_particle, Index), offsetof(mpi_particle, Mass), offsetof(mpi_particle, IntPos),
        offsetof(mpi_particle, Vel),   offsetof(mpi_particle, Spin), offsetof(mpi_particle, potential_energy_correction)};

    MPI_Type_create_struct(n_items, block_lengths, offsets, types, &dtype);
    MPI_Type_commit(&dtype);

    return dtype;
  }();

  return dtype;
}

mpi_particle mpi_particle::from_gadget_particle_index(const simparticles &Sp, int task, int index)
{
  const auto &p     = Sp.P[index];
  const double mass = p.getMass();
  const auto type   = p.getType();

  double zero_spin[3]     = {0, 0, 0};
  const double *spin_data = zero_spin;

  if(is_bh_type(type))
    {
      spin_data = p.Spin;
    }

  return {p.ID.get(),
          type,
          task,
          index,
          mass,
          {p.IntPos[0], p.IntPos[1], p.IntPos[2]},
          {p.Vel[0], p.Vel[1], p.Vel[2]},
          {spin_data[0], spin_data[1], spin_data[2]}};
}

/////////////////
// Data stored for BH mergers into the output file
struct bh_merger_data
{
  MyIDType ID1, ID2, ID_remnant;
  double m1, m2, m_remnant;
  double chi1, chi2, chi_remnant;
  double v_kick;    // kick velocity that is applied if kicks are enabled, stored even if it is not applied
  double t_merger;  // physical time of the merger
  double z_merger;  // redshift of the merger

  static MPI_Datatype get_mpi_datatype();
};

MPI_Datatype bh_merger_data::get_mpi_datatype()
{
  // construct the datatype on the first call
  static MPI_Datatype dtype = []() {
    MPI_Datatype dtype;
    const int n_items = 12;
    std::vector<int> block_lengths(n_items, 1);

    static_assert(std::is_same<MyIDType, unsigned int>::value || std::is_same<MyIDType, unsigned long long>::value);
    MPI_Datatype ID_type = std::is_same<MyIDType, unsigned int>::value ? MPI_UNSIGNED : MPI_UNSIGNED_LONG_LONG;

    MPI_Datatype types[] = {
        ID_type,    ID_type,    ID_type,    MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE,
        MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE,
    };
    MPI_Aint offsets[] = {offsetof(bh_merger_data, ID1),    offsetof(bh_merger_data, ID2),      offsetof(bh_merger_data, ID_remnant),
                          offsetof(bh_merger_data, m1),     offsetof(bh_merger_data, m2),       offsetof(bh_merger_data, m_remnant),
                          offsetof(bh_merger_data, chi1),   offsetof(bh_merger_data, chi2),     offsetof(bh_merger_data, chi_remnant),
                          offsetof(bh_merger_data, v_kick), offsetof(bh_merger_data, t_merger), offsetof(bh_merger_data, z_merger)};

    MPI_Type_create_struct(n_items, block_lengths.data(), offsets, types, &dtype);
    MPI_Type_commit(&dtype);

    return dtype;
  }();

  return dtype;
}

/////////////////

int parse_PN_terms()
{
  auto &terms = All.KetjuData.options.PN_terms;
  for(size_t i = 0; terms[i] != '\0'; ++i)
    {
      terms[i] = std::tolower(terms[i]);
      if(i >= sizeof terms)
        Terminate("Error: KetjuPNTerms too long for buffer!");
    }

  // special flags
  if(std::strcmp(terms, "all") == 0)
    return KETJU_PN_ALL;

  if(std::strcmp(terms, "no_spin") == 0)
    return KETJU_PN_DYNAMIC_ALL;

  if(std::strcmp(terms, "none") == 0)
    return KETJU_PN_NONE;

  int flags = KETJU_PN_NONE;
  for(int i = 0; terms[i] != '\0'; ++i)
    {
      switch(terms[i])
        {
          case '2':
            flags = flags | KETJU_PN_1_0_ACC;
            break;
          case '4':
            flags = flags | KETJU_PN_2_0_ACC;
            break;
          case '5':
            flags = flags | KETJU_PN_2_5_ACC;
            break;
          case '6':
            flags = flags | KETJU_PN_3_0_ACC;
            break;
          case '7':
            flags = flags | KETJU_PN_3_5_ACC;
            break;
          case 's':
            flags = flags | KETJU_PN_SPIN_ALL;
            break;
          case 'c':
            flags = flags | KETJU_PN_THREEBODY;  // cross terms
            break;
          default:
            char buf[50];
            std::snprintf(buf, sizeof buf, "Invalid character '%c' in KetjuPNTerms string!", terms[i]);
            Terminate(buf);
        }
    }
  return flags;
}

int get_PN_flags()
{
  static int res = parse_PN_terms();
  return res;
}

class IntegratorSystem : ketju_system
{
  struct extra_data
  {
    MyIDType ID;
    int Task;
    int Index;
    int Type;
  };
  int initial_num_particles;
  std::vector<extra_data> extra_data_storage;

 public:
  IntegratorSystem(int num_bhs, int num_other_particles, MPI_Comm comm)
      : initial_num_particles(num_bhs + num_other_particles), extra_data_storage(initial_num_particles)
  {
    ketju_create_system(this, num_bhs, num_other_particles, comm);
    particle_extra_data           = extra_data_storage.data();
    particle_extra_data_elem_size = sizeof(extra_data);
    constants->G                  = All.G;
    constants->c                  = CLIGHT / All.UnitVelocity_in_cm_per_s;

    options->PN_flags = get_PN_flags();

    const auto &opt                 = All.KetjuData.options;
    options->enable_bh_merger_kicks = opt.enable_bh_merger_kicks;
    options->star_star_softening    = opt.use_star_star_softening ? All.ForceSoftening[get_star_softening_class()] * All.cf_atime : 0;

    options->gbs_relative_tolerance         = opt.integration_relative_tolerance;
    options->output_time_relative_tolerance = opt.output_time_relative_tolerance;

    options->mst_algorithm                    = opt.use_divide_and_conquer_mst ? KETJU_MST_DIVIDE_AND_CONQUER_PRIM : KETJU_MST_PRIM;
    options->steps_between_mst_reconstruction = opt.steps_between_mst_reconstruction;
    options->max_tree_distance                = opt.max_tree_distance;

    options->max_step_count = (KETJU_INTEGRATOR_MAX_STEP_COUNT);
  }
  // We won't need these, even though they could be defined.
  // Make sure they're not available to avoid errors.
  IntegratorSystem(const IntegratorSystem &)            = delete;
  IntegratorSystem &operator=(const IntegratorSystem &) = delete;
  IntegratorSystem(IntegratorSystem &&)                 = delete;
  IntegratorSystem &operator=(IntegratorSystem &&)      = delete;

  ~IntegratorSystem() { ketju_free_system(this); }

  ketju_integration_options &get_options() { return *options; }
  const ketju_integration_options &get_options() const { return *options; }
  const ketju_performance_counters &get_perf() const { return *perf; }
  double get_time() const { return physical_state->time; }
  int get_num_pn_particles() const { return num_pn_particles; }
  int get_num_particles() const { return num_particles; }
  int get_num_mergers() const { return num_mergers; }
  const ketju_bh_merger_info *get_merger_info() const { return merger_infos; }
  void run(double integration_timespan) { ketju_run_integrator(this, integration_timespan); }

  // A simple class to allow accessing individual particles conveniently like single objects/structs,
  // even though the data is stored as a struct of arrays.
  template <typename IS>
  class ParticleProxyT
  {
    static_assert(std::is_same<typename std::remove_const<IS>::type, IntegratorSystem>::value);
    IS *const system;
    const int index;
    // Only allow construction through IntegratorSystem methods
    friend class IntegratorSystem;
    ParticleProxyT(IS *system, int index) : system(system), index(index) {}

   public:
    auto pos() -> double (&)[3] { return system->physical_state->pos[index]; }
    auto vel() -> double (&)[3] { return system->physical_state->vel[index]; }
    auto spin() -> double (&)[3]
    {
      if(index >= system->get_num_pn_particles())
        {
          Terminate("Out of bounds particle spin access!");
        }
      return system->physical_state->spin[index];
    }
    double &mass() { return system->physical_state->mass[index]; }
    MyIDType &ID() { return system->extra_data_storage[index].ID; }
    int &Task() { return system->extra_data_storage[index].Task; }
    int &Index() { return system->extra_data_storage[index].Index; }
    int &Type() { return system->extra_data_storage[index].Type; }

    auto pos() const -> const double (&)[3] { return system->physical_state->pos[index]; }
    auto vel() const -> const double (&)[3] { return system->physical_state->vel[index]; }
    auto spin() const -> const double (&)[3]
    {
      if(index >= system->get_num_pn_particles())
        {
          Terminate("Out of bounds particle spin access!");
        }
      return system->physical_state->spin[index];
    }
    const double &mass() const { return system->physical_state->mass[index]; }
    const MyIDType &ID() const { return system->extra_data_storage[index].ID; }
    const int &Task() const { return system->extra_data_storage[index].Task; }
    const int &Index() const { return system->extra_data_storage[index].Index; }
    const int &Type() const { return system->extra_data_storage[index].Type; }
  };

  using ParticleProxy      = ParticleProxyT<IntegratorSystem>;
  using ConstParticleProxy = ParticleProxyT<const IntegratorSystem>;

  ParticleProxy operator[](int index)
  {
    // Allow accessing merged particle data at the end of the array as well.
    if(index >= initial_num_particles)
      {
        Terminate("Out of bounds particle access!");
      }
    return ParticleProxy(this, index);
  }

  const ConstParticleProxy operator[](int index) const
  {
    if(index >= initial_num_particles)
      {
        Terminate("Out of bounds particle access!");
      }
    return ConstParticleProxy(this, index);
  }
};

//////////////////

class mpi_task_group
{
  // Default state is similar to what we get when current task is not a part of the group
  MPI_Group group = MPI_GROUP_NULL;
  MPI_Comm comm   = MPI_COMM_NULL;
  int rank        = MPI_UNDEFINED;
  int size        = 0;
  int root        = MPI_UNDEFINED;
  int root_sim    = MPI_UNDEFINED;

 public:
  mpi_task_group() = default;
  mpi_task_group(const std::vector<int> &task_sim_indices, int tag = 0)
  {
    MPI_Group sim_group;
    MPI_Comm_group(Shmem.SimulationComm, &sim_group);
    MPI_Group_incl(sim_group, task_sim_indices.size(), task_sim_indices.data(), &group);
    MPI_Group_size(group, &size);
    MPI_Group_rank(group, &rank);
    MPI_Comm_create_group(Shmem.SimulationComm, group, tag, &comm);
    MPI_Group_free(&sim_group);

    root     = 0;
    root_sim = task_sim_indices[root];
  }

  mpi_task_group(const mpi_task_group &)            = delete;
  mpi_task_group &operator=(const mpi_task_group &) = delete;

  mpi_task_group &operator=(mpi_task_group &&other) noexcept
  {
    std::swap(group, other.group);
    std::swap(comm, other.comm);

    rank     = other.rank;
    size     = other.size;
    root     = other.root;
    root_sim = other.root_sim;
    return *this;
  }
  mpi_task_group(mpi_task_group &&other) noexcept { *this = std::move(other); }

  ~mpi_task_group()
  {
    if(comm != MPI_COMM_NULL)
      MPI_Comm_free(&comm);
    if(group != MPI_GROUP_NULL)
      MPI_Group_free(&group);
  }

  int get_rank() const { return rank; }
  int get_size() const { return size; }
  int get_root() const { return root; }
  int get_root_sim() const { return root_sim; }
  MPI_Group get_group() const { return group; }
  MPI_Comm get_comm() const { return comm; }

  // Set both groups to have the same root task if possible
  void set_common_root(mpi_task_group &other)
  {
    if(group == MPI_GROUP_NULL || other.group == MPI_GROUP_NULL)
      return;

    MPI_Group sim_group;
    MPI_Comm_group(Shmem.SimulationComm, &sim_group);

    MPI_Group intersection;
    MPI_Group_intersection(group, other.group, &intersection);
    if(intersection != MPI_GROUP_EMPTY)
      {
        // found overlapping tasks, lets just set the first of them as the
        // root for both groups
        const int root_intersection_rank = 0;
        MPI_Group_translate_ranks(intersection, 1, &root_intersection_rank, group, &root);
        MPI_Group_translate_ranks(intersection, 1, &root_intersection_rank, other.group, &other.root);

        // Find the sim ranks of the root tasks
        MPI_Group_translate_ranks(group, 1, &root, sim_group, &root_sim);
        MPI_Group_translate_ranks(other.group, 1, &other.root, sim_group, &other.root_sim);
      }
    // nothing to do if no overlap between the groups

    MPI_Group_free(&intersection);
    MPI_Group_free(&sim_group);
  }
};

////////////////////

struct region_compute_info
{
  int compute_sequence_position;
  int first_task_index;
  int final_task_index;
};

// A single Ketju region.
class Region
{
  int region_index;
  // Set of particle indices on the local task within this region
  std::set<int> local_member_indices;

  // Data of the contained BHs before integration
  std::vector<mpi_particle> bhs;

  int timestep_region_index = -1;

  int total_particle_count;

  // CoM position, velocity in Gadget coordinates
  MyIntPosType CoM_IntPos[3];
  double CoM_Vel[3];

  double system_scale_fac;  // scale factor at the system's integrated time
  double system_hubble;     // hubble parameter likewise

  mpi_task_group affected_tasks, compute_tasks;

  std::vector<int> particle_counts_on_affected_tasks;
  std::vector<int> affected_task_sim_indices;

  region_compute_info compute_info;

  // Integrator data is only allocated on the compute tasks
  std::unique_ptr<IntegratorSystem> integrator_system;

  std::vector<mpi_particle> output_bh_data;
  std::vector<bh_merger_data> output_merger_data;

  void gadget_to_integrator_pos(const intposconvert &conv, const MyIntPosType (&gadget_pos)[3], double (&integrator_pos)[3]) const;
  void integrator_to_gadget_pos(const intposconvert &conv, const double (&integrator_pos)[3], MyIntPosType (&gadget_pos)[3]) const;
  void gadget_to_integrator_vel(const double (&gadget_vel)[3], const double (&integrator_pos)[3], double (&integrator_vel)[3]) const;
  void integrator_to_gadget_vel(const double (&integrator_vel)[3], const double (&integrator_pos)[3], double (&gadget_vel)[3]) const;

  std::vector<mpi_particle> get_particle_data_on_compute_tasks(const simparticles &Sp) const;
  void set_CoM_from_particles(const intposconvert &conv, const std::vector<mpi_particle> &particles);

  void do_negative_halfstep_kick(double halfstep_kick_factor);
  void expand_tight_binaries(double timestep);
  int setup_output_storage(const integertime region_Ti_step, const integertime output_Ti_step);
  void store_output_data_on_compute_root(const intposconvert &conv, int &output_index);
  void store_merger_data_on_compute_root(integertime t0);
  void run_integration(int timebin, const intposconvert &conv);

  std::vector<double> get_potential_energy_corrections_on_compute_root() const;
  std::vector<mpi_particle> get_particle_data_on_affected_root(const intposconvert &) const;

 public:
  Region(std::vector<mpi_particle> &&bhs, std::set<int> &&local_member_indices, int region_index)
      : region_index(region_index),
        local_member_indices{std::move(local_member_indices)},
        bhs{std::move(bhs)},
        system_scale_fac(All.cf_atime),
        system_hubble(All.cf_hubble_a)
  {
  }

  const std::vector<mpi_particle> &get_bhs() const { return bhs; }
  int get_bh_count() const { return bhs.size(); }
  const std::set<int> &get_local_member_indices() const { return local_member_indices; }

  void set_timestep_region_index(int i) { timestep_region_index = i; }
  int get_timestep_region_index() const { return timestep_region_index; }
  int get_region_index() const { return region_index; }
  int get_total_particle_count() const { return total_particle_count; }
  void set_compute_info(const region_compute_info &info) { compute_info = info; }
  const region_compute_info &get_compute_info() const { return compute_info; }
  int get_compute_sequence_position() const { return compute_info.compute_sequence_position; }
  int get_compute_root_sim() const { return compute_tasks.get_root_sim(); }
  bool this_task_is_in_compute_tasks() const { return compute_tasks.get_rank() != MPI_UNDEFINED; }
  bool this_task_is_compute_root() const { return compute_tasks.get_rank() == compute_tasks.get_root(); }
  bool this_task_is_affected_root() const { return affected_tasks.get_rank() == affected_tasks.get_root(); }
  bool this_task_is_in_affected_tasks() const { return affected_tasks.get_rank() != MPI_UNDEFINED; }
  const std::vector<mpi_particle> &get_output_bh_data() const { return output_bh_data; }
  const std::vector<bh_merger_data> &get_output_merger_data() const { return output_merger_data; }
  double get_normalized_integration_cost_from_compute_root(int timebin) const
  {
    if(!this_task_is_compute_root())
      return 0;

    return integrator_system->get_perf().total_work / (integertime(1) << timebin);
  }

  int get_max_timebin(const simparticles &Sp) const;
  void find_affected_tasks_and_particle_counts();
  void set_up_compute_comms();
  void set_up_integrator(const simparticles &Sp);
  void do_integration_step(int timebin, const intposconvert &conv);
  void update_sim_data(simparticles &Sp);
};

int Region::get_max_timebin(const simparticles &Sp) const
{
  double max_dt = std::min(TIMEBASE * All.Timebase_interval / All.cf_hubble_a, All.MaxSizeTimestep / All.cf_hubble_a);

  double stellar_com_vel[3] = {};
  double stellar_sigma      = 0;

  // Consider the motion of the CoM for limiting the timestep.
  // Generally the surrounding stellar system will place a stricter limit, but this guards against cases where it doesn't.
  // Also calculate the CoM vel and sigma of stellar particles for use further down.
  if(this_task_is_in_affected_tasks())
    {
      double com_data[7]           = {};  // vel + acc + mass
      double *vel_data             = com_data;
      double *acc_data             = com_data + 3;
      double stellar_vel_data[7]   = {};  // stellar com vel, vel^2, mass
      double *stellar_com_vel_data = stellar_vel_data;
      double *stellar_vel2_data    = stellar_vel_data + 3;
      for(int p : local_member_indices)
        {
          for(int k = 0; k < 3; ++k)
            {
              vel_data[k] += Sp.P[p].getMass() * Sp.P[p].Vel[k] / All.cf_atime;
              acc_data[k] += Sp.P[p].getMass() * Sp.P[p].GravAccel[k] * All.cf_a2inv;
#if defined(PMGRID) && !defined(TREEPM_NOTIMESPLIT)
              acc_data[k] += Sp.P[p].getMass() * Sp.P[p].GravPM[k] * All.cf_a2inv;
#endif
            }
          com_data[6] += Sp.P[p].getMass();

          if(is_star_type(Sp.P[p].getType()))
            {
              for(int k = 0; k < 3; ++k)
                {
                  stellar_com_vel_data[k] += Sp.P[p].getMass() * Sp.P[p].Vel[k] / All.cf_atime;
                  stellar_vel2_data[k] += Sp.P[p].getMass() * std::pow(Sp.P[p].Vel[k] / All.cf_atime, 2);
                }
              stellar_vel_data[6] += Sp.P[p].getMass();
            }
        }

      double com_res[7];
      MPI_Reduce(com_data, com_res, 7, MPI_DOUBLE, MPI_SUM, affected_tasks.get_root(), affected_tasks.get_comm());
      double stellar_vel_res[7];
      MPI_Reduce(stellar_vel_data, stellar_vel_res, 7, MPI_DOUBLE, MPI_SUM, affected_tasks.get_root(), affected_tasks.get_comm());
      if(this_task_is_affected_root())
        {
          const double com_mass = com_res[6];
          const double com_vel  = vector_norm(com_res) / com_mass;
          const double com_acc  = vector_norm(com_res + 3) / com_mass;

          // Check the acceleration time step criterion.
          // Use the same criterion as for softened particles,
          // but with a tenth of the ketju region radius as the softening
          // for a more conservative timestep.
          const double acc_dt = std::sqrt(0.2 * All.ErrTolIntAccuracy * All.KetjuData.options.region_physical_radius / com_acc);

          // Limit also so that the CoM of the system doesn't move too much relative to the region size
          const double com_vel_dt = 0.1 * All.KetjuData.options.region_physical_radius / com_vel;

          ketju_debug_printf("Region %d: max_dt=%g, acc_dt=%g, com_vel_dt=%g, ", region_index, max_dt, acc_dt, com_vel_dt);

          max_dt = std::min(max_dt, acc_dt);
          max_dt = std::min(max_dt, com_vel_dt);

          const double stellar_com_mass = stellar_vel_res[6];
          if(stellar_com_mass > 0)
            {  // Leave the stellar com vel and sigma as 0 if no stars in the region
              stellar_sigma = 0;
              for(int k = 0; k < 3; ++k)
                {
                  stellar_com_vel[k] = stellar_vel_res[k] / stellar_com_mass;
                  stellar_sigma += stellar_vel_res[k + 3] / stellar_com_mass - std::pow(stellar_com_vel[k], 2);
                }
              stellar_sigma /= 3;
              stellar_sigma = std::sqrt(stellar_sigma);
            }
        }
    }

  if(this_task_is_affected_root())
    {
      // A fairly conservative estimate based on BH velocities:
      // limit so that any BH only travels at most 30% of the region radius
      // relative to the stellar CoM vel during a single step assuming constant velocity.
      // This avoids passing through regions of stars too quickly for the stars to enter
      // the region, either due to the BH or stellar system motion, while avoiding
      // unnecessary limiting due to e.g. the whole galaxy moving.
      // In the outer 30% of the region the softened gravity is still nearly Newtonian
      // even for the smallest possible region.
      // We assume suitable continuity, so that the values calculated from the stars
      // in the region currently are representative of the surroundings.
      // For tight binaries this condition might be too strict, but with typical parameters should
      // still allow for several orbits in a gadget timestep,
      // and automatically deals with possible large GW recoil kicks as well.
      double max_vel = 0;
      for(const auto &bh : bhs)
        {
          double vel_diff[3];
          for(int k = 0; k < 3; ++k)
            {
              vel_diff[k] = bh.Vel[k] / All.cf_atime - stellar_com_vel[k];
            }

          max_vel = std::max(max_vel, vector_norm(vel_diff));
        }
      const double max_bh_vel_dt = 0.3 * All.KetjuData.options.region_physical_radius / max_vel;
      max_dt                     = std::min(max_dt, max_bh_vel_dt);

      // Limit also so that 3 * (stellar sigma) only covers at most 50% of region radius.
      // This should ensure that essentially all stars become active in the buffer zone,
      // and also that most stars enter the region within the outer 30% for the same reasons as above.
      const double stellar_sigma_dt = .5 * All.KetjuData.options.region_physical_radius / (3 * stellar_sigma);
      max_dt                        = std::min(max_dt, stellar_sigma_dt);

      ketju_debug_printf("max_bh_vel_dt=%g, stellar_sigma_dt=%g\n", max_bh_vel_dt, stellar_sigma_dt);
    }

  MPI_Bcast(&max_dt, 1, MPI_DOUBLE, affected_tasks.get_root_sim(), Shmem.SimulationComm);

  integertime ti_step = max_dt / All.Timebase_interval;

  // copied from timestep.cc simparticles::get_timestep_bin since getting access to that here is inconvenient
  int bin = -1;
  if(ti_step == 0)
    return 0;

  if(ti_step == 1)
    Terminate("time-step of integer size 1 not allowed\n");

  while(ti_step)
    {
      bin++;
      ti_step >>= 1;
    }

  return bin;
}

void Region::find_affected_tasks_and_particle_counts()
{
  const int NTask    = Shmem.Sim_NTask;
  const int ThisTask = Shmem.Sim_ThisTask;
  std::vector<int> particle_counts(NTask);
  particle_counts[ThisTask] = local_member_indices.size();
  MPI_Allgather(MPI_IN_PLACE, 1, MPI_INT, particle_counts.data(), 1, MPI_INT, Shmem.SimulationComm);

  total_particle_count = 0;
  for(int i = 0; i < NTask; ++i)
    {
      if(particle_counts[i] > 0)
        {
          total_particle_count += particle_counts[i];
          particle_counts_on_affected_tasks.push_back(particle_counts[i]);
          affected_task_sim_indices.push_back(i);
        }
    }
  // set up communicator
  affected_tasks = mpi_task_group(affected_task_sim_indices);
}

void Region::set_up_compute_comms()
{
  affected_tasks = mpi_task_group(affected_task_sim_indices);
  std::vector<int> compute_task_indices(compute_info.final_task_index - compute_info.first_task_index + 1);
  std::iota(compute_task_indices.begin(), compute_task_indices.end(), compute_info.first_task_index);
  compute_tasks = mpi_task_group(compute_task_indices);

  compute_tasks.set_common_root(affected_tasks);
}

void Region::set_CoM_from_particles(const intposconvert &conv, const std::vector<mpi_particle> &particles)
{
  // To maintain the position resolution, calculate using position
  // offsets to the first particle.
  double M = 0;
  std::fill(std::begin(CoM_Vel), std::end(CoM_Vel), 0);
  double CoM_shift[3] = {0, 0, 0};
  for(const auto &p : particles)
    {
      M += p.Mass;
      double p_shift[3];
      conv.nearest_image_intpos_to_pos(p.IntPos, particles[0].IntPos, p_shift);
      for(int i = 0; i < 3; ++i)
        {
          CoM_Vel[i] += p.Mass * p.Vel[i];
          CoM_shift[i] += p.Mass * p_shift[i];
        }
    }
  for(int i = 0; i < 3; ++i)
    {
      CoM_Vel[i] /= M;
      CoM_shift[i] /= M;
    }
  MySignedIntPosType CoM_Int_shift[3];
  conv.pos_to_signedintpos(CoM_shift, CoM_Int_shift);
  for(int i = 0; i < 3; ++i)
    {
      CoM_IntPos[i] = particles[0].IntPos[i] + CoM_Int_shift[i];
    }
}

void Region::gadget_to_integrator_pos(const intposconvert &conv, const MyIntPosType (&gadget_pos)[3],
                                      double (&integrator_pos)[3]) const
{
  conv.nearest_image_intpos_to_pos(gadget_pos, CoM_IntPos, integrator_pos);

  if(All.ComovingIntegrationOn)
    {
      for(auto &c : integrator_pos)
        {
          c *= system_scale_fac;
        }
    }
}

void Region::integrator_to_gadget_pos(const intposconvert &conv, const double (&integrator_pos)[3],
                                      MyIntPosType (&gadget_pos)[3]) const
{
  double pos[3];
  for(int i = 0; i < 3; ++i)
    {
      pos[i] = integrator_pos[i] / system_scale_fac;
    }
  MySignedIntPosType intpos_shift[3];
  conv.pos_to_signedintpos(pos, intpos_shift);

  for(int i = 0; i < 3; ++i)
    {
      // This may wrap as intended in a periodic box, while the safety buffer regions around the particles should keep it from
      // happening in non-periodic runs.
      // TODO: Some toy examples like a very tight, eccentric binary may end up breaking this if the initial RegionLen value is very
      // small. Is there anything that can be done to prevent that?
      gadget_pos[i] = CoM_IntPos[i] + intpos_shift[i];
    }
  conv.constrain_intpos(gadget_pos);
}

void Region::gadget_to_integrator_vel(const double (&gadget_vel)[3], const double (&integrator_pos)[3],
                                      double (&integrator_vel)[3]) const
{
  if(All.ComovingIntegrationOn)
    {
      for(int i = 0; i < 3; ++i)
        {
          // Include the small Hubble flow contribution to the internal relative velocities.
          // Note that gadget_vel is not the comoving velocity, but the canonical momentum.
          integrator_vel[i] = (gadget_vel[i] - CoM_Vel[i]) / system_scale_fac + system_hubble * integrator_pos[i];
        }
    }
  else
    {
      for(int i = 0; i < 3; ++i)
        {
          integrator_vel[i] = gadget_vel[i] - CoM_Vel[i];
        }
    }
}

void Region::integrator_to_gadget_vel(const double (&integrator_vel)[3], const double (&integrator_pos)[3],
                                      double (&gadget_vel)[3]) const
{
  if(All.ComovingIntegrationOn)
    {
      for(int i = 0; i < 3; ++i)
        {
          gadget_vel[i] = (integrator_vel[i] - system_hubble * integrator_pos[i]) * system_scale_fac + CoM_Vel[i];
        }
    }
  else
    {
      for(int i = 0; i < 3; ++i)
        {
          gadget_vel[i] = integrator_vel[i] + CoM_Vel[i];
        }
    }
}

std::vector<mpi_particle> Region::get_particle_data_on_compute_tasks(const simparticles &Sp) const
{
  // Now the implementation doesn't make use of the shared memory between tasks.
  // TODO: This could be improved if performance requires.

  std::vector<mpi_particle> recv_particle_data;

  if(this_task_is_in_compute_tasks() || this_task_is_affected_root())
    {
      recv_particle_data.resize(total_particle_count);
    }

  if(this_task_is_in_affected_tasks())
    {
      // Gather particle data on affected group root
      std::vector<mpi_particle> send_particle_data;
      send_particle_data.reserve(local_member_indices.size());

      const int thistask = affected_tasks.get_rank();
      for(int i : local_member_indices)
        {
          send_particle_data.emplace_back(mpi_particle::from_gadget_particle_index(Sp, thistask, i));
        }
      std::vector<int> displs(particle_counts_on_affected_tasks.size());
      std::partial_sum(particle_counts_on_affected_tasks.begin(), particle_counts_on_affected_tasks.end() - 1, displs.begin() + 1);
      MPI_Gatherv(send_particle_data.data(), send_particle_data.size(), mpi_particle::get_mpi_datatype(), recv_particle_data.data(),
                  particle_counts_on_affected_tasks.data(), displs.data(), mpi_particle::get_mpi_datatype(), affected_tasks.get_root(),
                  affected_tasks.get_comm());
    }

  // Send data from affected root to compute root if needed
  if(!(this_task_is_affected_root() && this_task_is_compute_root()))
    {
      if(this_task_is_affected_root())
        {
          MPI_Send(recv_particle_data.data(), recv_particle_data.size(), mpi_particle::get_mpi_datatype(),
                   compute_tasks.get_root_sim(), region_index, Shmem.SimulationComm);
        }
      if(this_task_is_compute_root())
        {
          MPI_Recv(recv_particle_data.data(), recv_particle_data.size(), mpi_particle::get_mpi_datatype(),
                   affected_tasks.get_root_sim(), region_index, Shmem.SimulationComm, MPI_STATUS_IGNORE);
        }
    }

  if(this_task_is_compute_root())
    {
      // Sort particle data to have BHs at the start, in
      // descending order by their masses.
      // The integrator requires BHs to be at the start of the data,
      // and ordering by masses is needed to support the minimum mass cutoff for
      // PN enabled BHs and also gives more logical merger remnant IDs.
      std::sort(recv_particle_data.begin(), recv_particle_data.end(), [](const mpi_particle &a, const mpi_particle &b) {
        if(is_bh_type(a.Type) && is_bh_type(b.Type))
          {
            return a.Mass > b.Mass;
          }

        return is_bh_type(a.Type);
      });
    }

  if(this_task_is_in_compute_tasks())
    {
      MPI_Bcast(recv_particle_data.data(), recv_particle_data.size(), mpi_particle::get_mpi_datatype(), compute_tasks.get_root(),
                compute_tasks.get_comm());
    }

  return recv_particle_data;
}

void Region::set_up_integrator(const simparticles &Sp)
{
  std::vector<mpi_particle> particle_data = get_particle_data_on_compute_tasks(Sp);

  if(!this_task_is_in_compute_tasks())
    return;

  set_CoM_from_particles(Sp, particle_data);

  const int num_bhs = get_bh_count();
  integrator_system =
      std::unique_ptr<IntegratorSystem>(new IntegratorSystem(num_bhs, total_particle_count - num_bhs, compute_tasks.get_comm()));

  for(int i = 0; i < total_particle_count; ++i)
    {
      auto &&p_int = (*integrator_system)[i];
      auto &p_gad  = particle_data[i];

      p_int.mass()  = p_gad.Mass;
      p_int.ID()    = p_gad.ID;
      p_int.Task()  = p_gad.Task;
      p_int.Index() = p_gad.Index;
      p_int.Type()  = p_gad.Type;

      gadget_to_integrator_pos(Sp, p_gad.IntPos, p_int.pos());
      gadget_to_integrator_vel(p_gad.Vel, p_int.pos(), p_int.vel());

      if(i < num_bhs)
        {
          std::copy(std::begin(p_gad.Spin), std::end(p_gad.Spin), std::begin(p_int.spin()));
        }
    }
}

// Schedule a block of work consisting of two loops of 0 <= i < Nloop-1,
// i < j < Nloop and divide it approximately evenly among num_proc tasks.
// edge_index == e gives the starting point for
// e'th process and edge_index == e+1 gives the ending point (non-inclusive).
int loop_scheduling_block_edge(int Nloop, int num_proc, int edge_index)
{
  if(Nloop <= 0 || num_proc <= 0)
    Terminate("Invalid call to ketju::loop_scheduling_block_edge");

  if(num_proc >= Nloop)  // more tasks than available work
    num_proc = Nloop;

  // Handle edges explicitly to avoid any possible issues with rounding,
  // otherwise possibly being off by one from the optimal value isn't that important.
  if(edge_index <= 0)
    return 0;

  if(edge_index >= num_proc)
    return Nloop - 1;

  // Use floating point math to avoid intermediate overflows
  const double P = num_proc;
  const double N = Nloop;
  // Analytic solution for the edge position below which the total work is nearest to edge_index * N * (N - 1) / (2 * P)
  return std::ceil(N - 0.5 - std::sqrt(N * (N - 1) * (P - edge_index) / P + 0.25));
}

double softened_inverse_r3(double h, double r)
{
  const double rinv = 1. / r;

  // Copied from gravtree::get_gfactors_monopole since getting
  // access to the Sim.Gravtree object here seems too annoying.
  if(r > h)
    {
      return rinv * rinv * rinv;
    }
  else
    {
      double h_inv  = 1 / h;
      double h2_inv = h_inv * h_inv;
      double u      = r * h_inv;

      if(u < 0.5)
        {
          double u2 = u * u;
          return rinv * h2_inv * u * ((SOFTFAC1) + u2 * ((SOFTFAC2)*u + (SOFTFAC3)));
        }
      else
        {
          double u2 = u * u;
          double u3 = u2 * u;
          return rinv * h2_inv * u * ((SOFTFAC8) + (SOFTFAC9)*u + (SOFTFAC10)*u2 + (SOFTFAC11)*u3 + (SOFTFAC12) / u3);
        }
    }
}

void Region::do_negative_halfstep_kick(double halfstep_kick_factor)
{
  if(!this_task_is_in_compute_tasks())
    return;

  auto &syst           = *integrator_system;
  const int num_part   = syst.get_num_particles();
  const int loop_start = loop_scheduling_block_edge(num_part, compute_tasks.get_size(), compute_tasks.get_rank());
  const int loop_end   = loop_scheduling_block_edge(num_part, compute_tasks.get_size(), compute_tasks.get_rank() + 1);

  // seems there isn't an easy way to get an automatically managed 2d array, so need to use new/delete.
  auto dv = new double[num_part][3]();

  // Assumes that the softenings are constant in physical coordinates also in comoving integration,
  // the scale factor is only due to how the values are stored.
  const double h = All.ForceSoftening[get_star_softening_class()] * All.cf_atime;

  // Compute the kicks for this task
  for(int i = loop_start; i < loop_end; ++i)
    {
      for(int j = i + 1; j < num_part; ++j)
        {
          auto &&pi = syst[i];
          auto &&pj = syst[j];
          double dr[3];
          double r2 = 0;
          // Operating in CoM coordinates, so always
          // non-periodic.
          for(int k = 0; k < 3; ++k)
            {
              dr[k] = pi.pos()[k] - pj.pos()[k];
              r2 += dr[k] * dr[k];
            }
          double dt_G_per_soft_r3 = halfstep_kick_factor * All.G * softened_inverse_r3(h, std::sqrt(r2));
          for(int k = 0; k < 3; ++k)
            {
              dv[i][k] += pj.mass() * dt_G_per_soft_r3 * dr[k];
              dv[j][k] -= pi.mass() * dt_G_per_soft_r3 * dr[k];
            }
        }
    }

  // Collect results and write to the ketju_system state
  MPI_Allreduce(MPI_IN_PLACE, dv, 3 * num_part, MPI_DOUBLE, MPI_SUM, compute_tasks.get_comm());

  for(int i = 0; i < num_part; ++i)
    {
      auto &&p = syst[i];
      for(int k = 0; k < 3; ++k)
        {
          p.vel()[k] += dv[i][k];
        }
    }

  delete[] dv;
}

// some helpers for Region::expand_tight_binaries
struct region_binaries
{
  struct binary_particles
  {
    int i, j;
    double period;
  };

  std::vector<binary_particles> binaries;
  double min_BH_binary_period;
};

// Find the bound binaries in the system
region_binaries find_binaries(int loop_start, int loop_end, const IntegratorSystem &integrator_system)
{
  double min_BH_binary_period = DBL_MAX;
  std::vector<region_binaries::binary_particles> binaries;
  const int num_part = integrator_system.get_num_particles();

  for(int i = loop_start; i < loop_end; ++i)
    {
      for(int j = i + 1; j < num_part; ++j)
        {
          double dr[3];
          double r2 = 0;
          double dv[3];
          double v2       = 0;
          const auto &&pi = integrator_system[i];
          const auto &&pj = integrator_system[j];
          // Operating in CoM coordinates, so always
          // non-periodic.
          for(int k = 0; k < 3; ++k)
            {
              dr[k] = pi.pos()[k] - pj.pos()[k];
              r2 += dr[k] * dr[k];
              dv[k] = pi.vel()[k] - pj.vel()[k];
              v2 += dv[k] * dv[k];
            }

          double GM = All.G * (pi.mass() + pj.mass());
          // energy per reduced mass of the potential binary
          double E = .5 * v2 - GM / std::sqrt(r2);

          if(E > 0)
            continue;  // not bound

          double a      = -GM / (2 * E);
          double period = 2 * M_PI * a * std::sqrt(a / GM);

          if(is_bh_type(pi.Type()) && is_bh_type(pj.Type()))
            {
              // For BH binaries the only interesting thing is the minimum
              // period, we'll never touch them otherwise.
              if(period < min_BH_binary_period)
                {
                  min_BH_binary_period = period;
                }
              continue;
            }
          // Otherwise store the binary for further processing
          binaries.push_back({i, j, period});
        }
    }
  return {std::move(binaries), min_BH_binary_period};
}

struct binary_expansion_info
{
  double old_a, new_a, delta_v;
};

// Expand the binary by kicking the particles to double the period.
// This breaks energy conservation.
binary_expansion_info double_binary_period(IntegratorSystem::ParticleProxy &pi, IntegratorSystem::ParticleProxy &pj)
{
  double r2 = 0;
  double dv[3];
  double v2 = 0;
  for(int k = 0; k < 3; ++k)
    {
      double dr = pi.pos()[k] - pj.pos()[k];
      r2 += dr * dr;
      dv[k] = pi.vel()[k] - pj.vel()[k];
      v2 += dv[k] * dv[k];
    }

  const double M     = (pi.mass() + pj.mass());
  const double potE  = All.G * M / std::sqrt(r2);
  const double E     = .5 * v2 - potE;
  const double newE  = 0.63 * E;  // approx. the factor required for doubling the period (2^-2/3)
  const double new_v = std::sqrt(2 * (potE + newE));
  const double old_v = std::sqrt(v2);

  for(int k = 0; k < 3; ++k)
    {
      dv[k] *= (new_v - old_v) / old_v;
      pi.vel()[k] += dv[k] * pj.mass() / M;
      pj.vel()[k] -= dv[k] * pi.mass() / M;
    }

  const double a     = All.G * M / (-2 * E);
  const double new_a = All.G * M / (-2 * newE);
  return {a, new_a, new_v - old_v};
}

void Region::expand_tight_binaries(double timestep)
{
  const int num_part = integrator_system->get_num_particles();
  const int num_bh   = integrator_system->get_num_pn_particles();

  // Parallelize the calculation
  int loop_start = loop_scheduling_block_edge(num_part, compute_tasks.get_size(), compute_tasks.get_rank());
  int loop_end   = loop_scheduling_block_edge(num_part, compute_tasks.get_size(), compute_tasks.get_rank() + 1);

  if(All.KetjuData.options.use_star_star_softening)
    {
      // Only check binaries containing a BH.
      // This loop parallelization wastes some computing power, but this is very cheap in this case anyway
      loop_start = std::min(loop_start, num_bh);
      loop_end   = std::min(loop_end, num_bh);
    }

  auto found_binaries         = find_binaries(loop_start, loop_end, *integrator_system);
  auto &binaries              = found_binaries.binaries;
  double min_BH_binary_period = found_binaries.min_BH_binary_period;
  MPI_Allreduce(MPI_IN_PLACE, &min_BH_binary_period, 1, MPI_DOUBLE, MPI_MIN, compute_tasks.get_comm());

  // sort the binaries to find the shortest periods
  std::sort(binaries.begin(), binaries.end(),
            [](const decltype(binaries[0]) &a, const decltype(binaries[0]) &b) { return a.period < b.period; });
  struct
  {
    double period;
    int rank;
  } min_period_rank = {binaries.empty() ? DBL_MAX : binaries[0].period, compute_tasks.get_rank()};

  MPI_Allreduce(MPI_IN_PLACE, &min_period_rank, 1, MPI_DOUBLE_INT, MPI_MINLOC, compute_tasks.get_comm());
  const double min_period = min_period_rank.period;

  if(this_task_is_compute_root())
    {
      ketju_debug_printf(
          "expand_tight_binaries: region %d min_period = %g, "
          "timestep = %g\n",
          region_index, min_period, timestep);
    }

  // No need to expand the binary if the period is longer than the timestep or
  // the minimum BH binary period times the period factor
  const double fac = All.KetjuData.options.expand_tight_binaries_period_factor;
  if(min_period > fac * timestep || min_period > fac * min_BH_binary_period)
    {
      return;
    }

  // Decide if the binary with the smallest period needs to be expanded.
  // If there are less than a set limit of  particles with periods < 2*min_period,
  // the tightest binary should be an outlier that can be expanded to improve performance.
  // Only one binary per region is expanded at once.
  // They should be rare enough for there to be no need to, and it's safer to
  // limit the artificial fudging of the physics.

  int count = 0;
  for(const auto &binary : binaries)
    {
      if(binary.period > min_period * 2)
        {
          break;  // the binaries are sorted in ascending order
        }
      ++count;
    }
  MPI_Allreduce(MPI_IN_PLACE, &count, 1, MPI_INT, MPI_SUM, compute_tasks.get_comm());

  if(count > std::max(num_part / 100., 5.))
    {
      return;
    }

  // get the particle indices of the binary to expand
  int ij[2];
  if(min_period_rank.rank == compute_tasks.get_rank())
    {
      ij[0] = binaries[0].i;
      ij[1] = binaries[0].j;
    }
  MPI_Bcast(ij, 2, MPI_INT, min_period_rank.rank, compute_tasks.get_comm());

  auto &&pi = (*integrator_system)[ij[0]];
  auto &&pj = (*integrator_system)[ij[1]];

  auto info = double_binary_period(pi, pj);

  if(this_task_is_compute_root())
    {
      ketju_printf(
          "expanding excessively tight binary in region %d\n"
          "    Particles (ID %llu type %d) and (ID %llu type %d)\n"
          "    Old period %g, semimajor axis %g, new period %g,"
          " new semimajor axis %g, delta_v %g\n",
          region_index, static_cast<unsigned long long>(pi.ID()), pi.Type(), static_cast<unsigned long long>(pj.ID()), pj.Type(),
          min_period, info.old_a, 2 * min_period, info.new_a, info.delta_v);
    }
}

void Region::store_merger_data_on_compute_root(integertime t0)
{
  if(!this_task_is_compute_root())
    return;

  const int num_mergers = integrator_system->get_num_mergers();
  if(num_mergers == 0)
    return;

  const double phys_t0 = All.ComovingIntegrationOn ? Driftfac.get_cosmic_time(t0) : All.Time;
  const double z0      = 1. / All.cf_atime - 1;
  const double z1      = 1. / system_scale_fac - 1;

  const ketju_bh_merger_info *infos = integrator_system->get_merger_info();

  for(int m = 0; m < num_mergers; ++m)
    {
      auto &info = infos[m];

      MyIDType ID1 = (*integrator_system)[info.extra_index1].ID();
      MyIDType ID2 = (*integrator_system)[info.extra_index2].ID();

      output_merger_data.emplace_back();
      bh_merger_data &data = output_merger_data.back();

      data.ID1         = ID1;
      data.ID2         = ID2;
      data.ID_remnant  = ID1;
      data.m1          = info.m1;
      data.m2          = info.m2;
      data.m_remnant   = info.m_remnant;
      data.chi1        = info.chi1;
      data.chi2        = info.chi2;
      data.chi_remnant = info.chi_remnant;
      data.v_kick      = info.v_kick;
      // The time fields start from zero at the beginning of the step.
      data.t_merger = info.t_merger + phys_t0;
      // Approximate with a linear interpolation, should be accurate enough.
      data.z_merger = z0 + (z1 - z0) * info.t_merger / integrator_system->get_time();

      // Output data to stdout in addition to the main output file.
      ketju_printf(
          "BH merger in region %d:\n"
          "  Phys. time: %.5g (redshift %.5f) + %.5g \n"
          "  ID %llu + %llu -> %llu\n"
          "  mass %.4g + %.4g -> %.4g\n"
          "  chi %3.2f + %3.2f -> %3.2f\n"
          "  Kick velocity %.4g (kicks are %s)\n",
          region_index, phys_t0, z0, info.t_merger, static_cast<unsigned long long>(ID1), static_cast<unsigned long long>(ID2),
          static_cast<unsigned long long>(ID1), info.m1, info.m2, info.m_remnant, info.chi1, info.chi2, info.chi_remnant, info.v_kick,
          (All.KetjuData.options.enable_bh_merger_kicks ? "enabled" : "disabled"));
    }
}

int Region::setup_output_storage(const integertime region_Ti_step, const integertime output_Ti_step)
{
  // Output is stored at the points where output_timebin is synchronized.
  // Initial state only stored on the first step, otherwise it has been stored
  // at the end of the  the previous integration if needed.
  const bool output_step_synced_at_end_of_current_step =
      All.Ti_Current + region_Ti_step == (1 + All.Ti_Current / output_Ti_step) * output_Ti_step;

  int num_output_points = region_Ti_step / output_Ti_step;

  if(num_output_points == 0 && output_step_synced_at_end_of_current_step)
    {
      num_output_points = 1;
    }

  if(All.Ti_Current == 0)
    {
      num_output_points += 1;
    }

  if(this_task_is_compute_root())
    {
      output_bh_data.resize(num_output_points * get_bh_count());
    }

  return num_output_points;
}

// Stores the BH data to positions given by output_index and advances output_index
void Region::store_output_data_on_compute_root(const intposconvert &conv, int &output_index)
{
  if(!this_task_is_compute_root())
    return;

  // The size of the output data storage is based on the initial number of BHs
  const int num_output_points = output_bh_data.size() / get_bh_count();

  if(num_output_points <= output_index)
    Terminate("Ketju: incorrect store_output_data calls!");

  // but data is only stored for the currently existing BHs, obviously
  const int num_bhs = integrator_system->get_num_pn_particles();
  for(int i = 0; i < num_bhs; ++i)
    {
      auto &p_out        = output_bh_data[i * num_output_points + output_index];
      const auto &&p_int = (*integrator_system)[i];

      p_out.Mass  = p_int.mass();
      p_out.ID    = p_int.ID();
      p_out.Task  = p_int.Task();
      p_out.Index = p_int.Index();
      p_out.Type  = p_int.Type();

      integrator_to_gadget_pos(conv, p_int.pos(), p_out.IntPos);
      integrator_to_gadget_vel(p_int.vel(), p_int.pos(), p_out.Vel);
      auto &spin = p_int.spin();
      std::copy(std::begin(spin), std::end(spin), std::begin(p_out.Spin));
    }
  ++output_index;
}

void Region::run_integration(int timebin, const intposconvert &conv)
{
  if(!this_task_is_in_compute_tasks())
    return;

  const integertime t0             = All.Ti_Current;
  const integertime region_Ti_step = integertime(1) << timebin;
  const integertime t1             = t0 + region_Ti_step;

  const int output_timebin         = All.KetjuData.output_timebin;
  const integertime output_Ti_step = ((integertime)1 << output_timebin);

  const int num_output_points = setup_output_storage(region_Ti_step, output_Ti_step);

  int output_index = 0;

  // output initial state
  if(All.Ti_Current == 0)
    {
      store_output_data_on_compute_root(conv, output_index);
    }

  // Ti_step is set so that output is written at the end of each iteration,
  // unless we don't need to do further output on this integration step.
  const integertime Ti_step       = std::min(region_Ti_step, output_Ti_step);
  const bool need_to_store_output = num_output_points > output_index;

  for(integertime t_cur = t0; t_cur < t1; t_cur += Ti_step)
    {
      double scale_fac = 1., hubble = 0.;
      double dt, CoM_dt;
      if(All.ComovingIntegrationOn)
        {
          // The CoM is propagated in gadget coordinates
          CoM_dt = Driftfac.get_drift_factor(t_cur, t_cur + Ti_step);
          // The subsystems are propagated in physical time
          dt = Driftfac.get_cosmic_timestep(t_cur, t_cur + Ti_step);
          // factors at the end of the step
          scale_fac = All.TimeBegin * std::exp((t_cur + Ti_step) * All.Timebase_interval);
          hubble    = Driftfac.hubble_function(scale_fac);
        }
      else
        {
          dt = CoM_dt = Ti_step * All.Timebase_interval;
        }

      integrator_system->run(dt);

      system_scale_fac = scale_fac;
      system_hubble    = hubble;

      for(int k = 0; k < 3; ++k)
        {
          CoM_IntPos[k] += conv.pos_to_signedintpos(CoM_dt * CoM_Vel[k]);
        }

      if(need_to_store_output)
        {
          store_output_data_on_compute_root(conv, output_index);
        }
    }

  store_merger_data_on_compute_root(t0);

  // Some manual timer log manipulation to log the integrator internal timers as well.
  auto &perf          = integrator_system->get_perf();
  double compute_time = perf.time_force + perf.time_gbsextr;
  double comm_time    = perf.time_force_comm + perf.time_gbscomm;
  TIMER_ADD(CPU_KETJU_INTEGRATOR_COMPUTE, compute_time);
  TIMER_ADD(CPU_KETJU_INTEGRATOR_COMM, comm_time);
  TIMER_ADD(CPU_KETJU_INTEGRATOR, perf.time_total - (compute_time + comm_time));
  TIMER_ADD(CPU_KETJU_INTEGRATION, -perf.time_total);

  if(this_task_is_compute_root())
    {
      ketju_printf("integrated region %d with %d successful + %d rejected steps\n", region_index, perf.successful_steps,
                   perf.failed_steps);
    }
}

void Region::do_integration_step(int timebin, const intposconvert &conv)
{
  double timestep                    = All.Timebase_interval * (integertime(1) << timebin);
  double first_halfstep_kick_factor  = .5 * timestep;
  double second_halfstep_kick_factor = .5 * timestep;

  if(All.ComovingIntegrationOn)
    {
      integertime t0          = All.Ti_Current;
      integertime t1          = t0 + (integertime(1) << timebin);
      const double scale_fac0 = All.TimeBegin * std::exp(t0 * All.Timebase_interval);
      const double scale_fac1 = All.TimeBegin * std::exp(t1 * All.Timebase_interval);
      // We compute the removal of the internal force component from
      // the velocities in physical coordinates, giving an extra
      // constant scale factor multiplier to the kick factors used for
      // kicking the canonical momentum with comoving potentials.
      first_halfstep_kick_factor  = Driftfac.get_gravkick_factor(t0, t0 + (t1 - t0) / 2) * scale_fac0;
      second_halfstep_kick_factor = Driftfac.get_gravkick_factor(t0 + (t1 - t0) / 2, t1) * scale_fac1;
      timestep                    = Driftfac.get_cosmic_timestep(t0, t1);
    }

  if(this_task_is_compute_root())
    {
      ketju_printf(
          "integrating region %d around BH ID %llu with %lu BH(s), %d particle(s) "
          "on tasks %d-%d on timebin %d (physical timestep %g)\n",
          get_region_index(), static_cast<unsigned long long>(get_bhs()[0].ID), get_bh_count(), get_total_particle_count(),
          get_compute_info().first_task_index, get_compute_info().final_task_index, timebin, timestep);
    }

  do_negative_halfstep_kick(first_halfstep_kick_factor);
  if(All.KetjuData.options.expand_tight_binaries_period_factor > 0)
    {
      expand_tight_binaries(timestep);
    }
  run_integration(timebin, conv);
  do_negative_halfstep_kick(second_halfstep_kick_factor);
}

// difference between -1/r and the softened potential
double softened_inverse_r_correction(double h, double r)
{
  if(r > h)
    {
      return 0.;
    }
  else
    {
      const double h_inv = 1 / h;
      const double u     = r * h_inv;
      const double u2    = u * u;

      if(u < 0.5)
        {
          return -1. / r - h_inv * ((SOFTFAC4) + u2 * ((SOFTFAC5) + u2 * ((SOFTFAC6)*u + (SOFTFAC7))));
        }
      else
        {
          return -1. / r -
                 h_inv * ((SOFTFAC13) + (SOFTFAC14) / u + u2 * ((SOFTFAC1) + u * ((SOFTFAC15) + u * ((SOFTFAC16) + (SOFTFAC17)*u))));
        }
    }
}

std::vector<double> Region::get_potential_energy_corrections_on_compute_root() const
{
  if(!this_task_is_in_compute_tasks())
    return {};

  // Return values for the particle count before integration, so any merged particles just get zeros
  std::vector<double> potcorr(total_particle_count, 0);
  // Return just zeros if values not needed, easier than guarding every access
  // to the array.
#ifdef EVALPOTENTIAL
  auto &syst         = *integrator_system;
  const int num_part = syst.get_num_particles();

  int loop_start, loop_end, inner_loop_end;
  if(All.KetjuData.options.use_star_star_softening)
    {
      loop_start     = loop_scheduling_block_edge(num_part, compute_tasks.get_size(), compute_tasks.get_rank());
      loop_end       = loop_scheduling_block_edge(num_part, compute_tasks.get_size(), compute_tasks.get_rank() + 1);
      inner_loop_end = num_part;
    }
  else
    {
      // Need to only compute interactions involving BHs, star-star potential is correct already
      const int part_per_task = num_part / compute_tasks.get_size();
      const int remainder     = num_part % compute_tasks.get_size();
      loop_start              = part_per_task * compute_tasks.get_rank();
      loop_start += std::min(remainder, compute_tasks.get_rank());
      loop_end       = loop_start + part_per_task + (compute_tasks.get_rank() < remainder ? 1 : 0);
      inner_loop_end = syst.get_num_pn_particles();
    }

  const double h = All.ForceSoftening[get_star_softening_class()] * All.cf_atime;
  for(int i = loop_start; i < loop_end; ++i)
    {
      const int inner_loop_start = All.KetjuData.options.use_star_star_softening ? 0 : i + 1;
      for(int j = inner_loop_start; j < inner_loop_end; ++j)
        {
          if(i == j)  // only possible in the use_star_star_softening case
            continue;

          auto &&pi = syst[i];
          auto &&pj = syst[j];

          double r2 = 0;
          for(int k = 0; k < 3; ++k)
            {
              const double dr = pi.pos()[k] - pj.pos()[k];
              r2 += dr * dr;
            }
          const double corr = All.G * softened_inverse_r_correction(h, std::sqrt(r2));
          potcorr[i] += pj.mass() * corr;
          potcorr[j] += pi.mass() * corr;
        }
    }

  if(this_task_is_compute_root())
    {
      MPI_Reduce(MPI_IN_PLACE, potcorr.data(), potcorr.size(), MPI_DOUBLE, MPI_SUM, compute_tasks.get_root(),
                 compute_tasks.get_comm());
    }
  else
    {
      MPI_Reduce(potcorr.data(), nullptr, potcorr.size(), MPI_DOUBLE, MPI_SUM, compute_tasks.get_root(), compute_tasks.get_comm());
      potcorr.clear();
    }
#else
  (void)softened_inverse_r_correction;  // silence unused warning
#endif

  return potcorr;
}

void update_sim_particle(const mpi_particle &p, simparticles &Sp)
{
  auto &p_gad           = Sp.P[p.Index];
  p_gad.KetjuIntegrated = true;

  if(p_gad.ID.get() != p.ID)
    {
      Terminate("Ketju: particle indexing error!");
    }

  p_gad.setMass(p.Mass);
  if(p.Mass == 0)  // merged away particle, no need for other fields
    return;

  integertime t0        = All.Ti_Current;
  integertime t1        = t0 + (integertime(1) << p_gad.TimeBinGrav);
  const double driftfac = All.ComovingIntegrationOn ? Driftfac.get_drift_factor(t0, t1) : All.Timebase_interval * (t1 - t0);

  double diff_pos[3];
  Sp.nearest_image_intpos_to_pos(p.IntPos, p_gad.IntPos, diff_pos);

  for(int k = 0; k < 3; ++k)
    {
      // The velocity is set to a mean velocity so that after the drift step finishes the particle position is the one given by the
      // integrator within double precision rounding error. The positions aren't accurate to machine precision anyway, so such
      // small errors are no problem, and we avoid having to store the integrated position explicitly.
      p_gad.Vel[k] = diff_pos[k] / driftfac;
      // The final velocity is moved into the main Vel field after the leapfrog step is complete.
      p_gad.KetjuFinalVel[k] = p.Vel[k];
    }

  if(is_bh_type(p.Type) && p.Mass >= All.KetjuData.options.minimum_bh_mass)
    {
      std::copy(std::begin(p.Spin), std::end(p.Spin), std::begin(p_gad.Spin));
    }

#ifdef EVALPOTENTIAL
  p_gad.KetjuPotentialEnergyCorrection = p.potential_energy_correction;
#endif
}

std::vector<mpi_particle> Region::get_particle_data_on_affected_root(const intposconvert &conv) const
{
  std::vector<mpi_particle> full_particle_data;

  // Needs to be called here, since does collective comms
  std::vector<double> potcorrs = get_potential_energy_corrections_on_compute_root();

  if(this_task_is_compute_root())
    {
      const int num_bhs       = integrator_system->get_num_pn_particles();
      const int Npart_current = integrator_system->get_num_particles();

      full_particle_data.resize(total_particle_count);
      for(int i = 0; i < total_particle_count; ++i)
        {
          auto &p_send = full_particle_data[i];
          auto &&p_int = (*integrator_system)[i];

          p_send.ID    = p_int.ID();
          p_send.Task  = p_int.Task();
          p_send.Index = p_int.Index();
          p_send.Type  = p_int.Type();

          if(i >= Npart_current)
            {
              // We're looking at a merged away particle, so set the particle mass to zero.
              // No need for other fields beyond this.
              // These particles get deleted during domain decomposition in the domain_rearrange_particle_sequence function.
              p_send.Mass = 0;
            }
          else
            {
              p_send.Mass = p_int.mass();
            }

          integrator_to_gadget_pos(conv, p_int.pos(), p_send.IntPos);
          integrator_to_gadget_vel(p_int.vel(), p_int.pos(), p_send.Vel);
          if(i < num_bhs)
            {
              auto &spin = p_int.spin();
              std::copy(std::begin(spin), std::end(spin), std::begin(p_send.Spin));
            }
          p_send.potential_energy_correction = potcorrs[i];
        }

      // Send data from compute root to affected root if needed
      if(!this_task_is_affected_root())
        {
          MPI_Send(full_particle_data.data(), full_particle_data.size(), mpi_particle::get_mpi_datatype(),
                   affected_tasks.get_root_sim(), region_index, Shmem.SimulationComm);
          full_particle_data.clear();
        }
    }
  else if(this_task_is_affected_root())
    {
      full_particle_data.resize(total_particle_count);
      MPI_Recv(full_particle_data.data(), full_particle_data.size(), mpi_particle::get_mpi_datatype(), compute_tasks.get_root_sim(),
               region_index, Shmem.SimulationComm, MPI_STATUS_IGNORE);
    }

  return full_particle_data;
}

void Region::update_sim_data(simparticles &Sp)
{
  std::vector<mpi_particle> full_particle_data = get_particle_data_on_affected_root(Sp);

  if(!this_task_is_in_affected_tasks())
    return;

  std::vector<int> displs(affected_tasks.get_size());
  if(this_task_is_affected_root())
    {
      std::sort(full_particle_data.begin(), full_particle_data.end(),
                [](const mpi_particle &a, const mpi_particle &b) { return a.Task < b.Task; });

      std::partial_sum(particle_counts_on_affected_tasks.begin(), particle_counts_on_affected_tasks.end() - 1, displs.begin() + 1);
    }

  std::vector<mpi_particle> local_particle_data(particle_counts_on_affected_tasks[affected_tasks.get_rank()]);
  MPI_Scatterv(full_particle_data.data(), particle_counts_on_affected_tasks.data(), displs.data(), mpi_particle::get_mpi_datatype(),
               local_particle_data.data(), local_particle_data.size(), mpi_particle::get_mpi_datatype(), affected_tasks.get_root(),
               affected_tasks.get_comm());

  for(auto &p : local_particle_data)
    {
      update_sim_particle(p, Sp);
    }
}

//////////////////

class TimestepRegion
{
  int timebin = -1;
  std::set<int> limiting_particle_indices;
  std::set<int> limited_particle_indices;

 public:
  TimestepRegion(std::set<int> &&limited_particle_indices, std::set<int> &&limiting_particle_indices)
      : limiting_particle_indices{std::move(limiting_particle_indices)}, limited_particle_indices{std::move(limited_particle_indices)}
  {
  }

  const std::set<int> &get_limited_particle_indices() const { return limited_particle_indices; }
  const std::set<int> &get_limiting_particle_indices() const { return limiting_particle_indices; }

  void decrease_timebin(int new_timebin)
  {
    if(new_timebin < timebin || timebin == -1)
      {
        timebin = new_timebin;
      }
  }

  int get_timebin() const { return timebin; }
};

///////////////////////////

// Provides storage for the Regions and methods that the interface
// functions call into
class RegionManager
{
  std::vector<Region> regions;
  std::vector<TimestepRegion> timestep_regions;
  std::unordered_set<int> all_limited_particle_indices;

  std::unordered_map<MyIDType, double> region_previous_cost;

  int num_sequential_computations;

  double region_cost_estimate(int region_index) const;
  using parallel_task_allocation = std::unordered_map<int, region_compute_info>;
  //< maps region index to assigned task data
  parallel_task_allocation allocate_parallel_run(const std::vector<int> &region_indices, int run_index) const;
  std::vector<parallel_task_allocation> allocate_sequential_runs(int num_runs) const;
  double estimate_total_time(const std::vector<parallel_task_allocation> &alloc) const;

 public:
  const std::vector<Region> &get_regions() const { return regions; }
  const std::vector<TimestepRegion> &get_timestep_regions() const { return timestep_regions; }

  void find_regions(const simparticles &Sp);
  void find_timestep_regions(const simparticles &Sp);
  void set_region_timebins(const simparticles &Sp);
  bool is_particle_timestep_limited(int target) const;
  int set_limited_timebins(simparticles &Sp, int min_timebin, int push_down_flag);
  void set_output_timebin() const;
  void set_up_regions_for_integration(const simparticles &Sp);
  void allocate_compute_tasks();
  void integrate_regions(const intposconvert &conv);
  std::pair<std::vector<std::vector<mpi_particle>>, std::vector<int>> comm_and_get_output_data_on_root() const;
  std::vector<bh_merger_data> comm_and_get_merger_data_on_root() const;
  void update_sim_data(simparticles &Sp);

  void clear_data()
  {
    regions.clear();
    timestep_regions.clear();
    all_limited_particle_indices.clear();
  }
};

void bh_spin_sanity_check(int target, const simparticles &Sp)
{
  const double chi =
      vector_norm(Sp.P[target].Spin) / (All.G * std::pow(Sp.P[target].getMass(), 2)) * (CLIGHT / All.UnitVelocity_in_cm_per_s);

  if(!(chi <= 1))
    {
      char msg[200];
      std::snprintf(msg, sizeof msg, "Ketju: BH ID %llu has unphysical spin |chi| = %g, should have |chi| < 1.\n",
                    static_cast<unsigned long long>(Sp.P[target].ID.get()), chi);
      Terminate(msg);
    }
}

std::vector<mpi_particle> find_bhs(const simparticles &Sp)
{
  std::vector<mpi_particle> local_bhs;

  for(int i = 0; i < Sp.TimeBinsGravity.NActiveParticles; ++i)
    {
      const int target = Sp.TimeBinsGravity.ActiveParticleList[i];

      if(is_bh_type(Sp.P[target].getType()))
        {
          const double mass = Sp.P[target].getMass();
          if(mass == 0 || mass < All.KetjuData.options.minimum_bh_mass)
            continue;

          bh_spin_sanity_check(target, Sp);

          local_bhs.push_back(mpi_particle::from_gadget_particle_index(Sp, Shmem.Sim_ThisTask, target));
        }
    }

  const MPI_Comm comm = Shmem.SimulationComm;
  const int NTask     = Shmem.Sim_NTask;

  std::vector<int> bh_counts(NTask);
  const int bh_count = local_bhs.size();
  MPI_Allgather(&bh_count, 1, MPI_INT, bh_counts.data(), 1, MPI_INT, comm);

  std::vector<int> displs(NTask);
  std::vector<int> recvcounts(NTask);
  int num_bhs = 0;
  for(int i = 0; i < NTask; ++i)
    {
      displs[i]     = num_bhs;
      recvcounts[i] = bh_counts[i];
      num_bhs += bh_counts[i];
    }

  std::vector<mpi_particle> bhs(num_bhs);
  MPI_Allgatherv(local_bhs.data(), bh_count, mpi_particle::get_mpi_datatype(), bhs.data(), recvcounts.data(), displs.data(),
                 mpi_particle::get_mpi_datatype(), comm);
  return bhs;
}

// Center is a type that has an IntPos field
template <typename Center>
std::set<int> find_local_particles(const simparticles &Sp, const std::vector<Center> &centers, const double radius,
                                   const unsigned int type_flags)
{
  // Do a simple linear search through the particles, since the particle tree structures
  // are constructed and freed in the functions where they are used, so that we don't have
  // access to one for free here.
  // Constructing one would likely be at least as expensive as doing this simple search,
  // since we only deal with a few center points at any time.
  std::set<int> particle_indices;
  for(int i = 0; i < Sp.TimeBinsGravity.NActiveParticles; ++i)
    {
      const int target = Sp.TimeBinsGravity.ActiveParticleList[i];
      const auto type  = Sp.P[target].getType();
      if(!check_type_flag(type, type_flags) || Sp.P[target].getMass() <= 0)
        continue;

      for(const auto &c : centers)
        {
          double offset[3];
          Sp.nearest_image_intpos_to_pos(Sp.P[target].IntPos, c.IntPos, offset);

          if(vector_norm(offset) <= radius)
            {
              particle_indices.insert(target);
              break;
            }
        }
    }
  return particle_indices;
}

// The regions and timestep regions consist of unions of spheres with BHs at the
// centers, and finding the regions consists mainly of combining overlapping
// regions.

// Check for overlap of two regions defined by center points and distance
// required for overlap. For regions consisting of spheres
// overlap_distance=2*sphere_radius, but for handling timestep regions we need a
// slightly more general definition.
// Center is a type with an IntPos field, e.g. mpi_particle.
template <typename Center>
bool regions_overlap(const std::vector<Center> &reg1_centers, const std::vector<Center> &reg2_centers, double overlap_distance,
                     const intposconvert &conv)
{
  for(const Center &c1 : reg1_centers)
    {
      for(const Center &c2 : reg2_centers)
        {
          double offset[3];
          conv.nearest_image_intpos_to_pos(c1.IntPos, c2.IntPos, offset);

          if(vector_norm(offset) < overlap_distance)
            return true;
        }
    }
  return false;
}

// Do the actual combining.
// Needs to handle input regions with multiple centers for efficiently combining timestep regions where the regions we start with may
// already contain multiple BHs.
template <typename Center>
std::vector<std::vector<Center>> combine_overlapping_sphere_union_regions(const std::vector<std::vector<Center>> &region_centers,
                                                                          double overlap_distance, const intposconvert &conv)
{
  std::vector<std::vector<Center>> combined_region_centers;
  for(auto &this_region_centers : region_centers)
    {
      std::vector<decltype(combined_region_centers.begin())> regions_this_region_overlaps_iterators;
      for(auto it = combined_region_centers.begin(); it != combined_region_centers.end(); ++it)
        {
          if(regions_overlap(this_region_centers, *it, overlap_distance, conv))
            {
              regions_this_region_overlaps_iterators.push_back(it);
            }
        }

      if(regions_this_region_overlaps_iterators.empty())
        {
          combined_region_centers.emplace_back(this_region_centers);
        }
      else
        {
          // Merge all the regions this region overlaps.
          // Iterate in reverse to avoid iterator invalidation with erase
          auto &main_region       = *regions_this_region_overlaps_iterators.front();
          const auto rit_to_first = regions_this_region_overlaps_iterators.rend() - 1;
          for(auto rit = regions_this_region_overlaps_iterators.rbegin(); rit != rit_to_first; ++rit)
            {
              auto &reg_bhs = **rit;  // rit is an iterator to a vector of iterators
              main_region.insert(main_region.end(), reg_bhs.begin(), reg_bhs.end());
              combined_region_centers.erase(*rit);
            }
          main_region.insert(main_region.end(), this_region_centers.begin(), this_region_centers.end());
        }
    }
  return combined_region_centers;
}

// Overload for when there's only one center per initial region
template <typename Center>
std::vector<std::vector<Center>> combine_overlapping_sphere_union_regions(const std::vector<Center> &region_centers,
                                                                          double overlap_distance, const intposconvert &conv)
{
  std::vector<std::vector<Center>> wrapped_centers;
  std::transform(region_centers.begin(), region_centers.end(), std::back_inserter(wrapped_centers),
                 [](const Center &c) { return std::vector<Center>{c}; });
  return combine_overlapping_sphere_union_regions(wrapped_centers, overlap_distance, conv);
}

void RegionManager::find_regions(const simparticles &Sp)
{
  const double region_radius = All.KetjuData.options.region_physical_radius * All.cf_ainv;

  auto region_bh_lists = combine_overlapping_sphere_union_regions(find_bhs(Sp), 2 * region_radius, Sp);

  int region_index = 0;
  for(auto &reg_bhs : region_bh_lists)
    {
      // Sort BHs by mass (and ID) to make the most massive one the main BH used for identifying the region
      std::sort(reg_bhs.begin(), reg_bhs.end(), [](const mpi_particle &bh1, const mpi_particle &bh2) {
        if(bh1.Mass == bh2.Mass)
          return bh1.ID < bh2.ID;  // ascending ID for equal masses

        return bh1.Mass > bh2.Mass;  // descending by mass
      });

      auto local_particles = find_local_particles(Sp, reg_bhs, region_radius, region_member_flags);
      regions.emplace_back(std::move(reg_bhs), std::move(local_particles), region_index++);
    }

  for(auto &reg : regions)
    {
      reg.find_affected_tasks_and_particle_counts();
    }
}

void RegionManager::find_timestep_regions(const simparticles &Sp)
{
  const double region_radius = All.KetjuData.options.region_physical_radius * All.cf_ainv;

  // All particles that are not in the regularized regions but within timestep_limiting_radius are used for setting the limited
  // timesteps. The limited timestep is the minimum used within this region.
  const double timestep_limiting_radius = region_radius * (KETJU_TIMESTEP_LIMITING_RADIUS_FACTOR);
  // Limited timesteps are set for particles that can go into the regions but within
  // a larger radius, to ensure that they are active when they enter within region_radius.
  const double timestep_limited_radius = 2 * region_radius;

  struct region_index_bh_pos
  {
    int region_index;
    MyIntPosType IntPos[3];
  };
  std::vector<std::vector<region_index_bh_pos>> region_indices_bh_pos;
  region_indices_bh_pos.reserve(regions.size());
  for(const auto &reg : regions)
    {
      region_indices_bh_pos.emplace_back();
      auto &vec = region_indices_bh_pos.back();

      for(const auto &bh : reg.get_bhs())
        {
          vec.push_back({reg.get_region_index(), {bh.IntPos[0], bh.IntPos[1], bh.IntPos[2]}});
        }
    }

  // Timestep regions merged if the limited radius of one region falls inside
  // the limiting radius of another, since then the timesteps must be the same.
  auto combined_region_indices_bh_pos =
      combine_overlapping_sphere_union_regions(region_indices_bh_pos, timestep_limiting_radius + timestep_limited_radius, Sp);

  for(const auto &tsr : combined_region_indices_bh_pos)
    {
      // Limiting particles are the particles within the
      // timestep_limiting_radius but outside the actual regularized region.
      auto limiting_particles = find_local_particles(Sp, tsr, timestep_limiting_radius, all_particles_flags);

      for(const auto &ind_pos : tsr)
        {
          for(auto j : regions[ind_pos.region_index].get_local_member_indices())
            {
              limiting_particles.erase(j);
            }
        }

      timestep_regions.emplace_back(find_local_particles(Sp, tsr, timestep_limited_radius, region_member_flags),
                                    std::move(limiting_particles));

      for(const auto &ind_pos : tsr)
        {
          regions[ind_pos.region_index].set_timestep_region_index(timestep_regions.size() - 1);
        }

      const auto &limited_inds = timestep_regions.back().get_limited_particle_indices();
      all_limited_particle_indices.insert(limited_inds.begin(), limited_inds.end());
    }
}

void RegionManager::set_region_timebins(const simparticles &Sp)
{
  for(auto &reg : regions)
    {
      timestep_regions[reg.get_timestep_region_index()].decrease_timebin(reg.get_max_timebin(Sp));
    }

  for(auto &treg : timestep_regions)
    {
      integertime ti_step = integertime(1) << treg.get_timebin();
      for(int i : treg.get_limiting_particle_indices())
        {
          integertime ts = Sp.get_timestep_grav(i);
          // Note: if in future the BHs are coupled to the hydrodynamics (e.g. accretion),
          // the hydro timestep should also be considered here to  make sure that the BH velocity seen by hydrodynamics is the physical
          // one and not the one set to ensure correct drift behavior.
          if(ts < ti_step && ts > 0)
            {
              ti_step = ts;
            }
        }
      int timebin;
      Sp.timebins_get_bin_and_do_validity_checks(ti_step, &timebin, 0);
      MPI_Allreduce(MPI_IN_PLACE, &timebin, 1, MPI_INT, MPI_MIN, Shmem.SimulationComm);
      treg.decrease_timebin(timebin);
    }
}

void RegionManager::set_output_timebin() const
{
  // If timesteps for some BHs have already been written into the future, we
  // shouldn't adjust the output timebin before that time has been reached
  if(All.Ti_Current < All.KetjuData.final_written_Ti)
    return;
  if(regions.empty())
    return;

  int max_timebin = 0;
  for(auto &tr : timestep_regions)
    {
      if(tr.get_timebin() > max_timebin)
        {
          max_timebin = tr.get_timebin();
        }
    }

  double max_phys_dt;
  if(All.ComovingIntegrationOn)
    {
      integertime t0 = All.Ti_Current;
      integertime t1 = t0 + (integertime(1) << max_timebin);
      max_phys_dt    = Driftfac.get_cosmic_timestep(t0, t1);
    }
  else
    {
      max_phys_dt = (integertime(1) << max_timebin) * All.Timebase_interval;
    }

  const double bin_diff        = log2(max_phys_dt / All.KetjuData.options.output_time_interval);
  All.KetjuData.output_timebin = max_timebin - bin_diff;
  if(All.KetjuData.output_timebin < 0)
    {
      // Negative output timebins are a hassle and rarely needed, so limit to non-negative.
      All.KetjuData.output_timebin = 0;
    }

  if(Shmem.Sim_ThisTask == 0)
    {
      ketju_debug_printf("output_timebin = %d\n", All.KetjuData.output_timebin);
    }
}

bool RegionManager::is_particle_timestep_limited(int index) const
{
  return all_limited_particle_indices.find(index) != all_limited_particle_indices.end();
}

int RegionManager::set_limited_timebins(simparticles &Sp, int min_timebin, int push_down_flag)
{
  // This method works for both timestepping modes.
  // If HIERARCHICAL_GRAVITY is not set, min_timebin and push_down_flag are always 0,
  // and this method is only called once per step.
  // Otherwise this is called each time the hierarchical scheme moves to a lower timebin,
  // and we need to account for the possible global push down of the timebin.

  if(push_down_flag)
    {
      // Called from HIERARCHICAL_GRAVITY loop, push down occurs so set the timebins to at most min_timebin
      for(auto &treg : timestep_regions)
        treg.decrease_timebin(min_timebin);
    }

  // Set the particle timebins to the region bin if possible,
  // but no lower than min_timebin so that HIERARCHICAL_GRAVITY works correctly.
  int max_timebin = 0;
  for(auto &treg : timestep_regions)
    {
      const int timebin = std::max(min_timebin, treg.get_timebin());
      max_timebin       = std::max(max_timebin, timebin);

      for(int target : treg.get_limited_particle_indices())
        {
          Sp.TimeBinsGravity.timebin_move_particle(target, Sp.P[target].TimeBinGrav, timebin);
          Sp.P[target].TimeBinGrav = timebin;
        }
    }
  return max_timebin;
}

double RegionManager::region_cost_estimate(int region_index) const
{
  auto elem = region_previous_cost.find(regions[region_index].get_bhs()[0].ID);
  if(elem == region_previous_cost.end())
    {
      // The BH ID didn't have an entry in the map, so base on particle count.
      // The prefactor scales the value to assume around 20 steps which should
      // generally be of the right magnitude, and even if it's very wrong,
      // we only get poor performance for a single step.
      // No need to include timestep factors here.
      return 20. * std::pow(regions[region_index].get_total_particle_count(), 2);
    }
  return elem->second;
}

using index_cost_pair           = std::pair<int, double>;
auto cost_comp_func             = [](const index_cost_pair &a, const index_cost_pair &b) { return a.second < b.second; };
using index_cost_priority_queue = std::priority_queue<index_cost_pair, std::vector<index_cost_pair>, decltype(cost_comp_func)>;

RegionManager::parallel_task_allocation RegionManager::allocate_parallel_run(const std::vector<int> &region_indices,
                                                                             int run_index) const
{
  const int N = region_indices.size();
  std::vector<int> task_counts(N, 0);

  std::vector<double> reg_costs;
  index_cost_priority_queue pq(cost_comp_func);
  for(int i = 0; i < N; ++i)
    {
      reg_costs.push_back(region_cost_estimate(region_indices[i]));
      pq.push({i, std::numeric_limits<double>::max()});  // max initial priority ensures >=1 tasks per region
    }

  for(int n = 0; n < Shmem.Sim_NTask && !pq.empty(); ++n)
    {
      int i = pq.top().first;
      pq.pop();
      int reg = region_indices[i];

      task_counts[i] += 1;
      // The integrator can make efficient use of  N_particles / ~10 tasks,
      // at this point the scaling flattens as most of the time gets spent on
      // comms. The exact number depends on the machine, so the exact number
      // is user definable.
      // Adding more tasks than required can be worse for performance due
      // increased comms.
      if(All.KetjuData.options.minimum_particles_per_task <= double(regions[reg].get_total_particle_count()) / (task_counts[i] + 1))
        {
          pq.push({i, reg_costs[i] / task_counts[i]});
        }
    }

  parallel_task_allocation alloc;
  int allocated = 0;
  for(int i = 0; i < N; ++i)
    {
      alloc[region_indices[i]] = {run_index, allocated, allocated + task_counts[i] - 1};
      allocated += task_counts[i];
    }

  return alloc;
}

std::vector<RegionManager::parallel_task_allocation> RegionManager::allocate_sequential_runs(int num_runs) const
{
  const unsigned int NTask = Shmem.Sim_NTask;
  index_cost_priority_queue region_pq(cost_comp_func);
  index_cost_priority_queue run_pq(cost_comp_func);

  std::vector<std::vector<int>> regions_in_run(num_runs);
  const int num_regions = regions.size();
  for(int i = 0; i < num_regions; ++i)
    {
      region_pq.push({i, region_cost_estimate(i)});
    }
  for(int i = 0; i < num_runs; ++i)
    {
      run_pq.push({i, 0.});
    }

  while(!region_pq.empty())
    {
      int reg         = region_pq.top().first;
      int region_cost = region_pq.top().second;
      region_pq.pop();
      int run      = run_pq.top().first;
      int run_cost = run_pq.top().second;
      run_pq.pop();
      regions_in_run[run].push_back(reg);
      // Prioritize the runs with the least cost assigned,
      // and ensure a run doesn't get overfilled.
      if(regions_in_run[run].size() < NTask)
        run_pq.push({run, run_cost - region_cost});
    }

  std::vector<parallel_task_allocation> alloc;
  for(int i = 0; i < num_runs; ++i)
    {
      alloc.emplace_back(allocate_parallel_run(regions_in_run[i], i));
    }
  return alloc;
}

double RegionManager::estimate_total_time(const std::vector<parallel_task_allocation> &alloc) const
{
  const int particles_per_task_scaling_limit = 7;  // Estimate based on the data in the integrator code paper

  double tot_t = 0;

  for(auto &par_alloc : alloc)
    {
      double max_t = 0;
      for(auto &kv : par_alloc)
        {
          const int reg  = kv.first;
          int task_count = kv.second.final_task_index - kv.second.first_task_index + 1;
          if(task_count > regions[reg].get_total_particle_count() / particles_per_task_scaling_limit)
            {
              // The parallelization efficiency drops dramatically around
              // here, so account for it in the estimation.
              task_count = regions[reg].get_total_particle_count() / particles_per_task_scaling_limit;
              if(task_count < 1)
                task_count = 1;
            }
          const double t = region_cost_estimate(reg) / task_count;
          if(t > max_t)
            {
              max_t = t;
            }
        }

      tot_t += max_t;
    }
  return tot_t;
}

void RegionManager::allocate_compute_tasks()
{
  // Determine the estimated best division of tasks and number of
  // sequential runs by testing out the possibilities and estimating the times
  // they will take.
  const int num_regions = regions.size();
  const int NTask       = Shmem.Sim_NTask;

  // We can't have more regions than tasks computed in parallel
  const int min_num_runs = num_regions / NTask + (num_regions % NTask == 0 ? 0 : 1);

  double min_time = DBL_MAX;

  std::vector<parallel_task_allocation> best_alloc;

  for(int num_runs = min_num_runs; num_runs <= num_regions; ++num_runs)
    {
      std::vector<parallel_task_allocation> current_alloc = allocate_sequential_runs(num_runs);

      const double t = estimate_total_time(current_alloc);
      if(t < min_time)
        {
          min_time = t;
          std::swap(best_alloc, current_alloc);
        }
      else
        {
          // If the estimated performance didn't increase with an additional
          // sequential run, adding another isn't likely to help either.
          break;
        }
    }

  num_sequential_computations = best_alloc.size();

  for(auto &par_alloc : best_alloc)
    {
      for(auto &kv : par_alloc)
        {
          regions[kv.first].set_compute_info(kv.second);

          if(Shmem.Sim_ThisTask == 0)
            {
              ketju_debug_printf("region %d got tasks %d-%d seq pos %d cost %g\n", kv.first, kv.second.first_task_index,
                                 kv.second.final_task_index, kv.second.compute_sequence_position, region_cost_estimate(kv.first));
            }
        }
    }
}

void RegionManager::set_up_regions_for_integration(const simparticles &Sp)
{
  TIMER_START(CPU_KETJU_CONSTRUCTION);
  allocate_compute_tasks();

  for(auto &reg : regions)
    {
      reg.set_up_compute_comms();
      reg.set_up_integrator(Sp);
    }
  TIMER_STOP(CPU_KETJU_CONSTRUCTION);
}

void RegionManager::integrate_regions(const intposconvert &conv)
{
  TIMER_START(CPU_KETJU_INTEGRATION);
  for(int i = 0; i < num_sequential_computations; ++i)
    {
      for(auto &reg : regions)
        {
          if(reg.get_compute_sequence_position() != i)
            continue;
          if(!reg.this_task_is_in_compute_tasks())
            continue;

          const int timebin = timestep_regions[reg.get_timestep_region_index()].get_timebin();
          reg.do_integration_step(timebin, conv);
        }
    }
  TIMER_STOP(CPU_KETJU_INTEGRATION);

  // Store the integration cost for load-balancing
  std::vector<double> region_costs;
  for(auto &reg : regions)
    {
      region_costs.push_back(
          reg.get_normalized_integration_cost_from_compute_root(timestep_regions[reg.get_timestep_region_index()].get_timebin()));
    }
  // Sum the costs so that all the tasks agree on the value.
  MPI_Allreduce(MPI_IN_PLACE, region_costs.data(), region_costs.size(), MPI_DOUBLE, MPI_SUM, Shmem.SimulationComm);

  const int num_regions = regions.size();
  for(int i = 0; i < num_regions; ++i)
    {
      region_previous_cost[regions[i].get_bhs()[0].ID] = region_costs[i];
    }
}

std::pair<std::vector<std::vector<mpi_particle>>, std::vector<int>> RegionManager::comm_and_get_output_data_on_root() const
{
  // first communicate how many data points each region has
  std::vector<MPI_Request> send_count_reqs, recv_count_reqs;
  std::vector<int> data_counts, send_data_counts;

  const int num_regs           = regions.size();
  const bool this_task_is_root = Shmem.Sim_ThisTask == 0;

  if(this_task_is_root)
    {
      data_counts.resize(num_regs);
      recv_count_reqs.resize(num_regs);
    }

  for(int i = 0; i < num_regs; ++i)
    {
      auto &reg = regions[i];
      if(reg.this_task_is_compute_root())
        {
          send_data_counts.push_back(reg.get_output_bh_data().size());
          send_count_reqs.emplace_back();
          MPI_Isend(&send_data_counts.back(), 1, MPI_INT, 0, i, Shmem.SimulationComm, &send_count_reqs.back());
        }

      if(this_task_is_root)
        {
          MPI_Irecv(&data_counts[i], 1, MPI_INT, reg.get_compute_root_sim(), i, Shmem.SimulationComm, &recv_count_reqs[i]);
        }
    }

  if(!send_count_reqs.empty())
    {
      MPI_Waitall(send_count_reqs.size(), send_count_reqs.data(), MPI_STATUSES_IGNORE);
    }

  if(!recv_count_reqs.empty())
    {
      MPI_Waitall(recv_count_reqs.size(), recv_count_reqs.data(), MPI_STATUSES_IGNORE);
    }

  // Then the actual data
  std::vector<std::vector<mpi_particle>> recv_data;
  std::vector<int> writable_reg_indices;
  std::vector<MPI_Request> send_data_reqs, recv_data_reqs;
  for(int i = 0; i < num_regs; ++i)
    {
      auto &reg = regions[i];
      if(this_task_is_root)
        {
          if(data_counts[i] == 0)
            continue;

          recv_data.emplace_back(data_counts[i]);
          recv_data_reqs.emplace_back();
          MPI_Irecv(recv_data.back().data(), data_counts[i], mpi_particle::get_mpi_datatype(), reg.get_compute_root_sim(), i,
                    Shmem.SimulationComm, &recv_data_reqs.back());
          writable_reg_indices.push_back(i);
        }

      if(reg.this_task_is_compute_root())
        {
          auto &data     = reg.get_output_bh_data();
          const int size = data.size();

          if(size == 0)
            continue;

          send_data_reqs.emplace_back();
          MPI_Isend(data.data(), size, mpi_particle::get_mpi_datatype(), 0, i, Shmem.SimulationComm, &send_data_reqs.back());
        }
    }

  if(!send_data_reqs.empty())
    {
      MPI_Waitall(send_data_reqs.size(), send_data_reqs.data(), MPI_STATUSES_IGNORE);
    }

  if(!recv_data_reqs.empty())
    {
      MPI_Waitall(recv_data_reqs.size(), recv_data_reqs.data(), MPI_STATUSES_IGNORE);
    }

  return std::make_pair(std::move(recv_data), std::move(writable_reg_indices));
}

std::vector<bh_merger_data> RegionManager::comm_and_get_merger_data_on_root() const
{
  std::vector<MPI_Request> send_count_reqs, recv_count_reqs;
  std::vector<int> data_counts, send_data_counts;

  const int num_regs           = regions.size();
  const bool this_task_is_root = Shmem.Sim_ThisTask == 0;

  if(this_task_is_root)
    {
      data_counts.resize(num_regs);
      recv_count_reqs.resize(num_regs);
    }

  for(int i = 0; i < num_regs; ++i)
    {
      auto &reg = regions[i];
      if(reg.this_task_is_compute_root())
        {
          send_data_counts.push_back(reg.get_output_merger_data().size());
          send_count_reqs.emplace_back();
          MPI_Isend(&send_data_counts.back(), 1, MPI_INT, 0, i, Shmem.SimulationComm, &send_count_reqs.back());
        }

      if(this_task_is_root)
        {
          MPI_Irecv(&data_counts[i], 1, MPI_INT, reg.get_compute_root_sim(), i, Shmem.SimulationComm, &recv_count_reqs[i]);
        }
    }

  if(!send_count_reqs.empty())
    {
      MPI_Waitall(send_count_reqs.size(), send_count_reqs.data(), MPI_STATUSES_IGNORE);
    }

  if(!recv_count_reqs.empty())
    {
      MPI_Waitall(recv_count_reqs.size(), recv_count_reqs.data(), MPI_STATUSES_IGNORE);
    }

  const int total_count = std::accumulate(data_counts.begin(), data_counts.end(), 0);
  std::vector<bh_merger_data> recv_data(total_count);
  std::vector<MPI_Request> send_data_reqs, recv_data_reqs;

  int offset = 0;
  for(int i = 0; i < num_regs; ++i)
    {
      auto &reg = regions[i];
      if(this_task_is_root)
        {
          if(data_counts[i] == 0)
            continue;

          recv_data_reqs.emplace_back();
          MPI_Irecv(recv_data.data() + offset, data_counts[i], bh_merger_data::get_mpi_datatype(), reg.get_compute_root_sim(), i,
                    Shmem.SimulationComm, &recv_data_reqs.back());
          offset += data_counts[i];
        }

      if(reg.this_task_is_compute_root())
        {
          auto &data = reg.get_output_merger_data();
          int size   = data.size();

          if(size == 0)
            continue;

          send_data_reqs.emplace_back();
          MPI_Isend(data.data(), size, bh_merger_data::get_mpi_datatype(), 0, i, Shmem.SimulationComm, &send_data_reqs.back());
        }
    }

  if(!send_data_reqs.empty())
    {
      MPI_Waitall(send_data_reqs.size(), send_data_reqs.data(), MPI_STATUSES_IGNORE);
    }

  if(!recv_data_reqs.empty())
    {
      MPI_Waitall(recv_data_reqs.size(), recv_data_reqs.data(), MPI_STATUSES_IGNORE);
    }

  return recv_data;
}

void RegionManager::update_sim_data(simparticles &Sp)
{
  TIMER_START(CPU_KETJU_UPDATE);
  for(auto &reg : regions)
    {
      reg.update_sim_data(Sp);
    }
  TIMER_STOP(CPU_KETJU_UPDATE);
}

//////////////////

class OutputFile
{
  const char *bhs_group     = "/BHs";
  const char *timestep_dset = "/timesteps";
  const char *merger_dset   = "/mergers";

  hid_t file_id = -1;

  void init_hdf5_types();
  void init_hdf5_output_file();
  void open_file_on_root_if_needed();
  hid_t open_or_create_bh_dataset(MyIDType ID);

  // Timestep data is written separately from the BHs so that we don't need to
  // duplicate it for each BH, and instead just need a single index into the
  // array of timesteps. This saves some storage, but not that much since we
  // only have two fields to store per timestep at the moment.
  // The separation of the timestep data requires keeping track of the index
  // to the timestep array (All.KetjuData.next_tstep_index),
  // which takes some effort when BHs may have different timesteps and get written at different rates.
  // On the other hand, this allows uniquely identifying which BH data points match each other,
  // even in the case where some timesteps are duplicated after the run has been restarted
  // after crashing for some reason.
  // The design originates from an older GADGET-3 version of the code where we
  // had some other arrays that referenced the timestep data in addition to the
  // BHs, and deduplicating the timestep data was more useful.
  struct timestep_output_data
  {
    double physical_time;
    //< Physical time of the simulation.
    //  All.Time in non-comoving and the cosmic time computed with  Driftfac.get_cosmic_time in comoving integration.
    double scale_factor;
    // Cosmological scale factor, always 1 in non-comoving integration.
  };

  struct bh_output_data
  {
    double gadget_position[3];  // physical position / scale_factor
    double gadget_velocity[3];  // peculiar velocity * scale_factor
    double physical_position[3];
    // Physical position wrapped to the nearest image to the region main BH  position for periodic runs, allowing easy computation of
    // the distances between BHs in the same region. Equals gadget_position * scale_factor modulo periodic box size.
    double physical_velocity[3];
    // Physical velocity including hubble flow contribution
    // (hubble flow part = H * physical_position).
    double spin[3];
    double mass;
    int timestep_index;
    // Index of the current timestep in the global timestep table
    int num_particles_in_region, num_BHs_in_region;

    bh_output_data(const mpi_particle &bh, const intposconvert &conv, const MyIntPosType ref_intpos[3], int tstep_index,
                   int num_BHs_in_region, int num_particles_in_region, double hubble, double scale_fac);
  };

  // HDF5 library datatype definitions stored here.
  // Note that we use the same datatypes to describe the struct in memory and
  // file, since the conversion overhead can be quite significant.
  // This can cause padding bytes from the structs to be written to the file
  // if they are present, which uses some extra space and causes Valgrind to
  // complain. But the performance impact is not worth using separate packed
  // representations for writing.
  class hdf5_datatypes
  {
    hid_t vector;
    hid_t timestep_data;
    hid_t bh_data;
    hid_t merger_data;

   public:
    hdf5_datatypes();
    ~hdf5_datatypes();

    template <typename T>
    hid_t get() const;
  } datatypes;

  template <typename T>
  hsize_t append_single_element(hid_t dset, const T &data);

  void write_bh_datapoint(MyIDType ID, const bh_output_data &data);
  void write_merger_datapoint(const bh_merger_data &data);
  hsize_t write_timestep_datapoint(double physical_time, double scale_factor);
  void write_timestep_data(const RegionManager &RM);
  void write_bh_data(const RegionManager &RM, const intposconvert &conv);
  void write_merger_data(const RegionManager &RM);

  void flush()
  {
    if(file_id >= 0)
      H5Fflush(file_id, H5F_SCOPE_LOCAL);
  }

 public:
  ~OutputFile()
  {
    if(file_id >= 0)
      {
        H5Fclose(file_id);
      }
  }

  void write_output_data(const RegionManager &RM, const intposconvert &conv);
};

OutputFile::hdf5_datatypes::hdf5_datatypes()
{
  hsize_t dims[1] = {3};
  vector          = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, dims);

  timestep_data = H5Tcreate(H5T_COMPOUND, sizeof(timestep_output_data));
  H5Tinsert(timestep_data, "physical_time", HOFFSET(timestep_output_data, physical_time), H5T_NATIVE_DOUBLE);
  H5Tinsert(timestep_data, "scale_factor", HOFFSET(timestep_output_data, scale_factor), H5T_NATIVE_DOUBLE);

  bh_data = H5Tcreate(H5T_COMPOUND, sizeof(bh_output_data));
  H5Tinsert(bh_data, "gadget_position", HOFFSET(bh_output_data, gadget_position), vector);
  H5Tinsert(bh_data, "gadget_velocity", HOFFSET(bh_output_data, gadget_velocity), vector);
  H5Tinsert(bh_data, "physical_position", HOFFSET(bh_output_data, physical_position), vector);
  H5Tinsert(bh_data, "physical_velocity", HOFFSET(bh_output_data, physical_velocity), vector);
  H5Tinsert(bh_data, "spin", HOFFSET(bh_output_data, spin), vector);
  H5Tinsert(bh_data, "mass", HOFFSET(bh_output_data, mass), H5T_NATIVE_DOUBLE);
  H5Tinsert(bh_data, "timestep_index", HOFFSET(bh_output_data, timestep_index), H5T_NATIVE_INT);
  H5Tinsert(bh_data, "num_particles_in_region", HOFFSET(bh_output_data, num_particles_in_region), H5T_NATIVE_INT);
  H5Tinsert(bh_data, "num_BHs_in_region", HOFFSET(bh_output_data, num_BHs_in_region), H5T_NATIVE_INT);

  static_assert(std::is_same<MyIDType, unsigned int>::value || std::is_same<MyIDType, unsigned long long>::value);
  const auto ID_type = std::is_same<MyIDType, unsigned int>::value ? H5T_NATIVE_UINT : H5T_NATIVE_ULLONG;

  merger_data = H5Tcreate(H5T_COMPOUND, sizeof(bh_merger_data));
  H5Tinsert(merger_data, "ID1", HOFFSET(bh_merger_data, ID1), ID_type);
  H5Tinsert(merger_data, "ID2", HOFFSET(bh_merger_data, ID2), ID_type);
  H5Tinsert(merger_data, "ID_remnant", HOFFSET(bh_merger_data, ID_remnant), ID_type);
  H5Tinsert(merger_data, "m1", HOFFSET(bh_merger_data, m1), H5T_NATIVE_DOUBLE);
  H5Tinsert(merger_data, "m2", HOFFSET(bh_merger_data, m2), H5T_NATIVE_DOUBLE);
  H5Tinsert(merger_data, "m_remnant", HOFFSET(bh_merger_data, m_remnant), H5T_NATIVE_DOUBLE);
  H5Tinsert(merger_data, "chi1", HOFFSET(bh_merger_data, chi1), H5T_NATIVE_DOUBLE);
  H5Tinsert(merger_data, "chi2", HOFFSET(bh_merger_data, chi2), H5T_NATIVE_DOUBLE);
  H5Tinsert(merger_data, "chi_remnant", HOFFSET(bh_merger_data, chi_remnant), H5T_NATIVE_DOUBLE);
  H5Tinsert(merger_data, "kick_velocity", HOFFSET(bh_merger_data, v_kick), H5T_NATIVE_DOUBLE);
  H5Tinsert(merger_data, "merger_physical_time", HOFFSET(bh_merger_data, t_merger), H5T_NATIVE_DOUBLE);
  H5Tinsert(merger_data, "merger_redshift", HOFFSET(bh_merger_data, z_merger), H5T_NATIVE_DOUBLE);
}

OutputFile::hdf5_datatypes::~hdf5_datatypes()
{
  H5Tclose(vector);
  H5Tclose(timestep_data);
  H5Tclose(bh_data);
  H5Tclose(merger_data);
}

template <typename T>
hid_t OutputFile::hdf5_datatypes::get() const
{
  static_assert("Invalid type passed");
  return 0;
}

template <>
hid_t OutputFile::hdf5_datatypes::get<OutputFile::bh_output_data>() const
{
  return bh_data;
}

template <>
hid_t OutputFile::hdf5_datatypes::get<bh_merger_data>() const
{
  return merger_data;
}

template <>
hid_t OutputFile::hdf5_datatypes::get<OutputFile::timestep_output_data>() const
{
  return timestep_data;
}

template <>
hid_t OutputFile::hdf5_datatypes::get<double[3]>() const
{
  return vector;
}

void OutputFile::init_hdf5_output_file()
{
  {  // Local scopes for different datasets/attributes, to prevent reusing the same variables by mistake
    // Cosmology
    struct cosmology_data
    {
      double HubbleParam;
      double Omega0;
      double OmegaLambda;
      double OmegaBaryon;
      int ComovingIntegrationOn;
      int Periodic;
      double BoxSize[3];
    } cosmo = {All.HubbleParam,
               All.Omega0,
               All.OmegaLambda,
               All.OmegaBaryon,
               All.ComovingIntegrationOn,
#ifdef PERIODIC
               1,
               {All.BoxSize / LONG_X, All.BoxSize / LONG_Y, All.BoxSize / LONG_Z}
#else
               0,
               {0, 0, 0}
#endif
    };

    hid_t datatype = H5Tcreate(H5T_COMPOUND, sizeof(cosmology_data));
    H5Tinsert(datatype, "HubbleParam", HOFFSET(cosmology_data, HubbleParam), H5T_NATIVE_DOUBLE);
    H5Tinsert(datatype, "Omega0", HOFFSET(cosmology_data, Omega0), H5T_NATIVE_DOUBLE);
    H5Tinsert(datatype, "OmegaLambda", HOFFSET(cosmology_data, OmegaLambda), H5T_NATIVE_DOUBLE);
    H5Tinsert(datatype, "OmegaBaryon", HOFFSET(cosmology_data, OmegaBaryon), H5T_NATIVE_DOUBLE);
    H5Tinsert(datatype, "ComovingIntegrationOn", HOFFSET(cosmology_data, ComovingIntegrationOn), H5T_NATIVE_INT);
    H5Tinsert(datatype, "Periodic", HOFFSET(cosmology_data, Periodic), H5T_NATIVE_INT);
    H5Tinsert(datatype, "BoxSize", HOFFSET(cosmology_data, BoxSize), datatypes.get<double[3]>());

    hid_t space = H5Screate(H5S_SCALAR);
    hid_t attr  = H5Acreate(file_id, "cosmology", datatype, space, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attr, datatype, &cosmo);

    H5Aclose(attr);
    H5Sclose(space);
    H5Tclose(datatype);
  }

  {
    // Units
    struct unit_data
    {
      double unit_time_in_s, unit_length_in_cm, unit_mass_in_g, unit_velocity_in_cm_per_s, G_cgs, c_cgs, timebase_interval;
    } units = {All.UnitTime_in_s,
               All.UnitLength_in_cm,
               All.UnitMass_in_g,
               All.UnitVelocity_in_cm_per_s,
               All.G / (All.UnitMass_in_g / std::pow(All.UnitVelocity_in_cm_per_s, 2) / All.UnitLength_in_cm),
               CLIGHT,
               All.Timebase_interval};

    hid_t datatype = H5Tcreate(H5T_COMPOUND, sizeof(unit_data));
    H5Tinsert(datatype, "unit_time_in_s", HOFFSET(unit_data, unit_time_in_s), H5T_NATIVE_DOUBLE);
    H5Tinsert(datatype, "unit_length_in_cm", HOFFSET(unit_data, unit_length_in_cm), H5T_NATIVE_DOUBLE);
    H5Tinsert(datatype, "unit_mass_in_g", HOFFSET(unit_data, unit_mass_in_g), H5T_NATIVE_DOUBLE);
    H5Tinsert(datatype, "unit_velocity_in_cm_per_s", HOFFSET(unit_data, unit_velocity_in_cm_per_s), H5T_NATIVE_DOUBLE);
    H5Tinsert(datatype, "G_cgs", HOFFSET(unit_data, G_cgs), H5T_NATIVE_DOUBLE);
    H5Tinsert(datatype, "c_cgs", HOFFSET(unit_data, c_cgs), H5T_NATIVE_DOUBLE);
    H5Tinsert(datatype, "timebase_interval", HOFFSET(unit_data, timebase_interval), H5T_NATIVE_DOUBLE);

    hid_t space = H5Screate(H5S_SCALAR);
    hid_t attr  = H5Acreate(file_id, "units", datatype, space, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attr, datatype, &units);

    H5Aclose(attr);
    H5Sclose(space);
    H5Tclose(datatype);
  }

  {
    // Integrator options
    hid_t datatype = H5Tcreate(H5T_COMPOUND, sizeof(Options));
    H5Tinsert(datatype, "minimum_bh_mass", HOFFSET(Options, minimum_bh_mass), H5T_NATIVE_DOUBLE);
    H5Tinsert(datatype, "region_physical_radius", HOFFSET(Options, region_physical_radius), H5T_NATIVE_DOUBLE);
    H5Tinsert(datatype, "output_time_interval", HOFFSET(Options, output_time_interval), H5T_NATIVE_DOUBLE);

    hid_t pn_string = H5Tcopy(H5T_C_S1);
    H5Tset_size(pn_string, sizeof Options::PN_terms);
    H5Tinsert(datatype, "PN_terms", HOFFSET(Options, PN_terms), pn_string);
    H5Tclose(pn_string);

    H5Tinsert(datatype, "enable_bh_merger_kicks", HOFFSET(Options, enable_bh_merger_kicks), H5T_NATIVE_INT);
    H5Tinsert(datatype, "use_star_star_softening", HOFFSET(Options, use_star_star_softening), H5T_NATIVE_INT);
    H5Tinsert(datatype, "expand_tight_binaries_period_factor", HOFFSET(Options, expand_tight_binaries_period_factor),
              H5T_NATIVE_DOUBLE);

    H5Tinsert(datatype, "integration_relative_tolerance", HOFFSET(Options, integration_relative_tolerance), H5T_NATIVE_DOUBLE);
    H5Tinsert(datatype, "output_time_relative_tolerance", HOFFSET(Options, output_time_relative_tolerance), H5T_NATIVE_DOUBLE);

    H5Tinsert(datatype, "minimum_particles_per_task", HOFFSET(Options, minimum_particles_per_task), H5T_NATIVE_DOUBLE);

    H5Tinsert(datatype, "use_divide_and_conquer_mst", HOFFSET(Options, use_divide_and_conquer_mst), H5T_NATIVE_INT);
    H5Tinsert(datatype, "max_tree_distance", HOFFSET(Options, max_tree_distance), H5T_NATIVE_INT);
    H5Tinsert(datatype, "steps_between_mst_reconstruction", HOFFSET(Options, steps_between_mst_reconstruction), H5T_NATIVE_INT);

    hid_t space = H5Screate(H5S_SCALAR);
    hid_t attr  = H5Acreate(file_id, "integrator options", datatype, space, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attr, datatype, &All.KetjuData.options);

    H5Aclose(attr);
    H5Sclose(space);
    H5Tclose(datatype);
  }

  {
    // Create data group for BHs
    hid_t group = H5Gcreate(file_id, bhs_group, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Gclose(group);
  }

  {
    // Create dataset for timestep data
    hsize_t dims[1] = {0}, maxdims[1] = {H5S_UNLIMITED};
    hsize_t chunk_dims[1] = {4096 / sizeof(timestep_output_data)};
    hid_t space           = H5Screate_simple(1, dims, maxdims);
    hid_t prop            = H5Pcreate(H5P_DATASET_CREATE);
    H5Pset_chunk(prop, 1, chunk_dims);
    hid_t dset = H5Dcreate(file_id, timestep_dset, datatypes.get<timestep_output_data>(), space, H5P_DEFAULT, prop, H5P_DEFAULT);
    H5Pclose(prop);
    H5Dclose(dset);
    H5Sclose(space);
  }

  {
    // Create dataset for merger data
    hsize_t dims[1] = {0}, maxdims[1] = {H5S_UNLIMITED};
    hsize_t chunk_dims[1] = {1};
    hid_t space           = H5Screate_simple(1, dims, maxdims);
    hid_t prop            = H5Pcreate(H5P_DATASET_CREATE);
    H5Pset_chunk(prop, 1, chunk_dims);
    hid_t dset = H5Dcreate(file_id, merger_dset, datatypes.get<bh_merger_data>(), space, H5P_DEFAULT, prop, H5P_DEFAULT);
    H5Pclose(prop);
    H5Dclose(dset);
    H5Sclose(space);
  }
}

void OutputFile::open_file_on_root_if_needed()
{
  if(Shmem.Sim_ThisTask != 0)
    return;

  if(file_id >= 0)
    return;

  char fname[550];
  std::snprintf(fname, sizeof fname, "%s/ketju_bhs.hdf5", All.OutputDir);

  if(All.RestartFlag == RST_RESUME || All.RestartFlag == RST_STARTFROMSNAP)
    {
      // Try to open the existing file
      file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);
      if(file_id < 0)
        {
          ketju_printf(
              "Could not open file '%s' for appending, will attempt "
              "creating a new file\n",
              fname);
        }
    }
  if(All.RestartFlag == RST_BEGIN || file_id < 0)
    {
      // Create a new output file when starting from scratch or when
      // opening the previous output file failed
      file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
      init_hdf5_output_file();
    }
  if(file_id < 0)
    {
      char msg[700];
      std::snprintf(msg, sizeof msg, "error in opening file '%s'\n", fname);
      Terminate(msg);
    }
}

hid_t OutputFile::open_or_create_bh_dataset(MyIDType ID)
{
  char dset_name[20];
  std::snprintf(dset_name, sizeof dset_name, "%s/%llu", bhs_group, static_cast<unsigned long long>(ID));
  if(H5Lexists(file_id, dset_name, H5P_DEFAULT))
    {
      hid_t dset = H5Dopen(file_id, dset_name, H5P_DEFAULT);
      return dset;
    }

  // need to create the dataset
  hsize_t dims[1] = {0}, maxdims[1] = {H5S_UNLIMITED};
  hid_t space           = H5Screate_simple(1, dims, maxdims);
  hid_t prop            = H5Pcreate(H5P_DATASET_CREATE);
  hsize_t chunk_dims[1] = {4096 / sizeof(bh_output_data)};
  H5Pset_chunk(prop, 1, chunk_dims);

  hid_t dset = H5Dcreate(file_id, dset_name, datatypes.get<bh_output_data>(), space, H5P_DEFAULT, prop, H5P_DEFAULT);
  H5Pclose(prop);
  H5Sclose(space);
  return dset;
}

template <typename T>
hsize_t OutputFile::append_single_element(hid_t dset, const T &data)
{
  hid_t space = H5Dget_space(dset);
  hsize_t dims[1];
  H5Sget_simple_extent_dims(space, dims, NULL);
  H5Sclose(space);
  hsize_t index = dims[0];
  dims[0] += 1;
  H5Dset_extent(dset, dims);
  space = H5Dget_space(dset);

  hsize_t offset[1] = {index};
  hsize_t count[1]  = {1};
  H5Sselect_hyperslab(space, H5S_SELECT_SET, offset, NULL, count, NULL);

  // Need to use a separate dataspace for the memory, since the dtype_id
  // constructed at the beginning of the program differs from the one for
  // reopened files/datasets.
  hid_t memspace = H5Screate_simple(1, count, count);

  H5Dwrite(dset, datatypes.get<T>(), memspace, space, H5P_DEFAULT, &data);

  H5Sclose(memspace);
  H5Sclose(space);
  return index;
}

void OutputFile::write_bh_datapoint(MyIDType ID, const bh_output_data &data)
{
  hid_t dset = open_or_create_bh_dataset(ID);
  append_single_element(dset, data);
  H5Dclose(dset);
}

void OutputFile::write_merger_datapoint(const bh_merger_data &data)
{
  hid_t dset = H5Dopen(file_id, merger_dset, H5P_DEFAULT);
  append_single_element(dset, data);
  H5Dclose(dset);
}

// returns the current index to the timestep table
hsize_t OutputFile::write_timestep_datapoint(double physical_time, double scale_factor)
{
  timestep_output_data data = {physical_time, scale_factor};
  hid_t dset                = H5Dopen(file_id, timestep_dset, H5P_DEFAULT);
  hsize_t index             = append_single_element(dset, data);
  H5Dclose(dset);
  return index;
}

void OutputFile::write_timestep_data(const RegionManager &RM)
{
  // First set the final_written_Ti value, needed on all tasks for setting the output timebin
  int max_timebin = 0;
  for(auto &tr : RM.get_timestep_regions())
    {
      if(tr.get_timebin() > max_timebin)
        {
          max_timebin = tr.get_timebin();
        }
    }

  const int output_timebin         = All.KetjuData.output_timebin;
  const integertime output_Ti_step = integertime(1) << output_timebin;

  // Nothing to do if the next output time is larger than the one reached by
  // the highest current bin
  if(All.Ti_Current > 0 &&
     (All.Ti_Current + (((integertime)1) << max_timebin)) < (1 + All.Ti_Current / output_Ti_step) * output_Ti_step)
    return;

  // The final time that is written is set by the highest region timebin,
  // which will be in sync with the output timebin by the above check.
  const integertime final_Ti_to_write = All.Ti_Current + (((integertime)1) << max_timebin);

  if(All.KetjuData.final_written_Ti >= final_Ti_to_write)
    return;

  All.KetjuData.final_written_Ti = final_Ti_to_write;

  // Actual writing on root only
  const bool this_task_is_root = Shmem.Sim_ThisTask == 0;
  if(!this_task_is_root)
    return;

  int tstep_index        = -10;  // We should always write something, this catches errors if we don't
  int num_tsteps_written = 0;
  // Initial Ti is the next output time we haven't written yet
  integertime Ti = (1 + All.Ti_Current / output_Ti_step) * output_Ti_step;
  if(All.Ti_Current == 0)
    Ti = 0;
  for(; Ti <= final_Ti_to_write; Ti += output_Ti_step)
    {
      double current_time, scale_factor;
      if(All.ComovingIntegrationOn)
        {
          current_time = Driftfac.get_cosmic_time(Ti);
          scale_factor = All.TimeBegin * std::exp(Ti * All.Timebase_interval);
        }
      else
        {
          current_time = All.TimeBegin + Ti * All.Timebase_interval;
          scale_factor = 1;
        }
      tstep_index = write_timestep_datapoint(current_time, scale_factor);
      ++num_tsteps_written;
    }

  // Flag to handle the possible error below specially on the first
  // write after restarting.
  static bool first_write = true;

  if(tstep_index - num_tsteps_written + 1 != All.KetjuData.next_tstep_index)
    {
      char buf[100];
      std::snprintf(buf, sizeof buf,
                    "Ketju: inconsistent output tstep index, expected %d "
                    "actual %d",
                    All.KetjuData.next_tstep_index, tstep_index - num_tsteps_written + 1);
      if(first_write && All.RestartFlag != RST_BEGIN)
        {
          // It seems the error is most likely caused by restarting
          // after the code crashed (or using restartfiles older than
          // the output file for some other reason).  In this case
          // there's extra data in the file the index isn't aware of,
          // but it can be safely ignored by adjusting the index. The
          // sections of duplicated data will just need to be handled
          // when reading.
          std::printf("%s\n", buf);
          ketju_printf(
              "it looks like this is caused by restarting"
              " after a crash, so adjusting and continuing.\n");
          All.KetjuData.next_tstep_index = tstep_index - num_tsteps_written + 1;
        }
      else
        {
          // Something else has gone wrong, either a bug or an
          // unaccounted for special case requiring adjustment.
          Terminate(buf);
        }
    }
  first_write = false;
}

OutputFile::bh_output_data::bh_output_data(const mpi_particle &bh, const intposconvert &conv, const MyIntPosType ref_intpos[3],
                                           int tstep_index, int num_BHs_in_region, int num_particles_in_region, double hubble,
                                           double scale_fac)
    : mass(bh.Mass),
      timestep_index(tstep_index),
      num_particles_in_region(num_particles_in_region),
      num_BHs_in_region(num_BHs_in_region)
{
  conv.intpos_to_pos(bh.IntPos, gadget_position);

  std::copy(std::begin(bh.Vel), std::end(bh.Vel), std::begin(gadget_velocity));
  std::copy(std::begin(bh.Spin), std::end(bh.Spin), std::begin(spin));

  conv.nearest_image_intpos_to_pos(bh.IntPos, ref_intpos, physical_position);

  double ref_pos[3];
  conv.intpos_to_pos(ref_intpos, ref_pos);

  for(int l = 0; l < 3; ++l)
    {
      physical_position[l] += ref_pos[l];
      physical_position[l] *= scale_fac;
      physical_velocity[l] = bh.Vel[l] / scale_fac + hubble * physical_position[l];
    }
}

void OutputFile::write_bh_data(const RegionManager &RM, const intposconvert &conv)
{
  const auto output_data = RM.comm_and_get_output_data_on_root();

  const bool this_task_is_root = Shmem.Sim_ThisTask == 0;
  if(!this_task_is_root)
    return;

  const std::vector<std::vector<mpi_particle>> &writable_region_data = output_data.first;
  const std::vector<int> &writable_reg_indices                       = output_data.second;

  const int output_timebin         = All.KetjuData.output_timebin;
  const integertime output_Ti_step = integertime(1) << output_timebin;

  const int num_writable_regs = writable_region_data.size();
  for(int i = 0; i < num_writable_regs; ++i)
    {
      const auto &data   = writable_region_data[i];
      const auto &region = RM.get_regions()[writable_reg_indices[i]];

      const int num_initial_bhs   = region.get_bh_count();
      const int num_output_points = data.size() / num_initial_bhs;

      // time of the first datapoint
      const integertime first_Ti_out = (All.Ti_Current / output_Ti_step) * output_Ti_step + (All.Ti_Current == 0 ? 0 : output_Ti_step);

      int tstep_index = All.KetjuData.next_tstep_index;
      for(int j = 0; j < num_output_points; ++j, ++tstep_index)
        {
          double scale_fac = 1., hubble = 0.;
          if(All.ComovingIntegrationOn)
            {
              const integertime Ti_out = first_Ti_out + j * output_Ti_step;

              scale_fac = All.TimeBegin * std::exp(Ti_out * All.Timebase_interval);
              hubble    = Driftfac.hubble_function(scale_fac);
            }

          // There may have been a merger during the integration, which is
          // detected from zero data for the merged away BHs.
          int actual_num_bhs = 0;
          for(int k = 0; k < num_initial_bhs; ++k)
            {
              const auto &bh = data[j + num_output_points * k];
              if(bh.Mass > 0)
                ++actual_num_bhs;
            }

          const auto &ref_intpos            = data[j].IntPos;
          const int num_particles_in_region = region.get_total_particle_count() - num_initial_bhs + actual_num_bhs;

          for(int k = 0; k < num_initial_bhs; ++k)
            {
              const auto &bh = data[j + num_output_points * k];
              if(bh.Mass == 0)
                continue;  // Merged

              bh_output_data bh_data(bh, conv, ref_intpos, tstep_index, actual_num_bhs, num_particles_in_region, hubble, scale_fac);
              write_bh_datapoint(bh.ID, bh_data);
            }
        }
    }
}

void OutputFile::write_merger_data(const RegionManager &RM)
{
  const auto merger_data = RM.comm_and_get_merger_data_on_root();

  const bool this_task_is_root = Shmem.Sim_ThisTask == 0;
  if(!this_task_is_root)
    return;

  for(auto &merger : merger_data)
    {
      write_merger_datapoint(merger);
    }
}

void OutputFile::write_output_data(const RegionManager &RM, const intposconvert &conv)
{
  if(RM.get_regions().empty())
    return;

  open_file_on_root_if_needed();
  write_timestep_data(RM);
  write_bh_data(RM, conv);
  write_merger_data(RM);
  flush();
}

///////////////

// next_tstep_index is the index in the output file for the next datapoint to be
// written. To allow for new BHs that are created during the run, it needs to be
// advanced every time the output timebin is synced/active, regardless of if
// there are BHs that are integrated on this step.
// But if there aren't any BHs the index shouldn't be advanced beyond the final
// step index + 1. This is detected from the final_written_Ti check.
void advance_next_tstep_index()
{
  if(Shmem.Sim_ThisTask != 0)
    return;  // the value isn't needed

  // An extra datapoint is written for the initial state when Ti_Current == 0,
  // but this is accounted for in the timestep index progress only on the next
  // pass, so need to use this little trick.
  static bool start_point_handled = true;

  if(All.Ti_Current == 0)
    {
      start_point_handled = false;
      return;  // Start of the run, haven't written anything yet
    }

  if(!start_point_handled)
    {
      ++All.KetjuData.next_tstep_index;
      start_point_handled = true;
    }

  integertime output_Ti_step = integertime(1) << All.KetjuData.output_timebin;
  if(All.Ti_Current <= All.KetjuData.final_written_Ti && (All.Ti_Current % output_Ti_step == 0))
    {
      if(All.LowestOccupiedGravTimeBin > All.KetjuData.output_timebin)
        {
          const int substeps = 1 << (All.LowestOccupiedGravTimeBin - All.KetjuData.output_timebin);

          All.KetjuData.next_tstep_index += substeps;
        }
      else
        {
          All.KetjuData.next_tstep_index += 1;
        }
    }
}

void parameter_sanity_check()
{
  static bool checked = false;
  if(checked || Shmem.Sim_ThisTask != 0)
    return;
  checked = true;

  if(All.KetjuData.options.region_physical_radius <= 0)
    Terminate("Value of KetjuRegionPhysicalRadius must be positive");

  if(All.KetjuData.options.output_time_interval <= 0)
    Terminate("Value of KetjuOutputTimeInterval must be positive");

  if(All.KetjuData.options.expand_tight_binaries_period_factor < 0)
    Terminate("Value of KetjuExpandTightBinaries must be non-negative");

  if(All.KetjuData.options.integration_relative_tolerance <= 0)
    Terminate("Value of KetjuIntegrationRelativeTolerance must be positive");

  if(All.KetjuData.options.output_time_relative_tolerance <= 0)
    Terminate("Value of KetjuOutputTimeRelativeTolerance must be positive");

  // The region radius needs to be larger than the force softening kernel
  // (2.8 * softening lenght) of stars and BHs for the BHs to continuously
  // feel the Newtonian force from particles outside the region and to avoid
  // discontinuous jumps in force when particles enter the ketju region.

  for(int n = 1; n < NTYPES; ++n)
    {
      if(check_type_flag(n, region_member_flags))
        {
          if(All.SofteningClassOfPartType[n] != get_star_softening_class())
            {
              // It doesn't make sense to use different softenings for stars and BHs, since
              // BH softening doesn't end up mattering anyway and allowing for it just causes more cases to check in the code.
              // If multiple stellar particle types are used, they must have the same softening due to the integrator only supporting a
              // single softening length.
              Terminate(
                  "Ketju: All particle types specified in  KETJU_STAR_PARTICLE_TYPE_FLAGS and KETJU_BH_PARTICLE_TYPE_FLAGS (by "
                  "default 4 and 5) must use the same SofteningClass");
            }
        }
    }

  double max_soft;
  if(All.ComovingIntegrationOn)
    {
      // We demand that the MaxPhys limit is active from the start of the run.
      const double scale = All.TimeBegin;

      const double init_soft_stars     = All.SofteningComoving[get_star_softening_class()] * scale;
      const double max_phys_soft_stars = All.SofteningMaxPhys[get_star_softening_class()];

      if(max_phys_soft_stars > init_soft_stars)
        {
          Terminate(
              "Ketju: Softening lengths of stars and BHs must be constant in physical coordinates when KETJU is active."
              " Set SofteningMaxPhys so that it is <= SofteningComoving * TimeBegin.");
        }

      max_soft = max_phys_soft_stars;
    }
  else
    {
      max_soft = All.SofteningComoving[get_star_softening_class()];
    }

  const double min_radius = 2.8 * max_soft;

  const double region_radius = All.KetjuData.options.region_physical_radius;
  if(region_radius < min_radius)
    {
      char buf[300];
      std::snprintf(buf, sizeof buf,
                    "Ketju: KetjuRegionPhysicalRadius = %3.3g is too "
                    "small! "
                    "The region radius needs to be at least 2.8 times larger than "
                    "the star and BH (Bndry) physical softening lengths. Current "
                    "softening lengths require KetjuRegionPhysicalRadius > "
                    "%3.3g.",
                    region_radius, min_radius);
      Terminate(buf);
    }

#ifdef PERIODIC
  // The periodic box needs to be large enough to fit a reasonable amount of
  // overlapping ketju regions without the region wrapping completely around,
  // since that is not a situation we support.
  // now the relevant scale is the scale at the beginning of the run
  const double scale = All.ComovingIntegrationOn ? All.TimeBegin : 1;
  // Arbitrarily pick 10 radii as the minimum box size.
  const double min_box_region_ratio = 10;
  // The box appears to have only one length allowed with gravity, and the
  // BoxSize is probably the smallest side even if LONG_{X,Y,Z} were
  // enabled.
  if(All.BoxSize * scale < min_box_region_ratio * region_radius)
    {
      char buf[300];
      std::snprintf(buf, sizeof buf,
                    "Ketju: BoxSize = %3.3g too small compared to "
                    "KetjuRegionPhysicalRadius = %3.3g! Set BoxSize > %3.3g "
                    "or KetjuRegionPhysicalRadius < %3.3g.",
                    All.BoxSize, region_radius, min_box_region_ratio * region_radius / scale,
                    All.BoxSize * scale / min_box_region_ratio);
      Terminate(buf);
    }
#endif
}

///////////////////

// Objects that we only need (and can only have) a single one each.
// Only used directly in the interface functions below.
RegionManager RM;
OutputFile OF;

}  // namespace

//////////////////

// Interface functions, these are the only thing the rest of the code sees.
namespace ketju
{
void find_regions(const simparticles &Sp)
{
  TIMER_START(CPU_KETJU_CONSTRUCTION);

  parameter_sanity_check();
  RM.find_regions(Sp);
  RM.find_timestep_regions(Sp);
  RM.set_region_timebins(Sp);
  advance_next_tstep_index();
  RM.set_output_timebin();

  TIMER_STOP(CPU_KETJU_CONSTRUCTION);
}

bool is_particle_timestep_limited(int index) { return RM.is_particle_timestep_limited(index); }
int set_limited_timebins(simparticles &Sp, int min_timebin, int push_down_flag)
{
  TIMER_START(CPU_KETJU_MISC);
  const int max_bin = RM.set_limited_timebins(Sp, min_timebin, push_down_flag);
  TIMER_STOP(CPU_KETJU_MISC);
  return max_bin;
}

void run_integration(simparticles &Sp)
{
  TIMER_START(CPU_KETJU_MISC);

  RM.set_up_regions_for_integration(Sp);
  RM.integrate_regions(Sp);
  OF.write_output_data(RM, Sp);
  RM.update_sim_data(Sp);
  RM.clear_data();

  TIMER_STOP(CPU_KETJU_MISC);
}

void set_final_velocities(simparticles &Sp)
{
  TIMER_START(CPU_KETJU_UPDATE);
  for(int i = 0; i < Sp.TimeBinsGravity.NActiveParticles; ++i)
    {
      auto &p = Sp.P[Sp.TimeBinsGravity.ActiveParticleList[i]];
      if(p.KetjuIntegrated)
        {
          // Active particles are drifted to the current time in simparticles::make_list_of_active_particles(),
          // so no need to check for that.
          std::copy(std::begin(p.KetjuFinalVel), std::end(p.KetjuFinalVel), std::begin(p.Vel));
        }
    }
  TIMER_STOP(CPU_KETJU_UPDATE);
}

void finish_step(simparticles &Sp)
{
#ifdef EVALPOTENTIAL
  if(All.NumCurrentTiStep == 0)
    {
      // A hack to get the correct potential value on the first output before the first actual step has started.
      // Construct regions etc. to get the potential computation done within the RM.update_sim_data() call,
      // but don't set timebins, integrate, or touch outputfiles.
      // The uninitialized timestep data is not needed in the methods called below.
      // The particle data should otherwise stay unchanged after this,
      // apart from possible rounding errors when moving to the region CoM frame and back.
      TIMER_START(CPU_KETJU_MISC);
      RM.find_regions(Sp);
      RM.set_up_regions_for_integration(Sp);
      RM.update_sim_data(Sp);
      RM.clear_data();
      set_final_velocities(Sp);
      TIMER_STOP(CPU_KETJU_MISC);
    }
#endif

  TIMER_START(CPU_KETJU_UPDATE);
  for(int i = 0; i < Sp.TimeBinsGravity.NActiveParticles; ++i)
    {
      auto &p = Sp.P[Sp.TimeBinsGravity.ActiveParticleList[i]];
      if(p.KetjuIntegrated)
        {
#ifdef EVALPOTENTIAL
          p.Potential += p.KetjuPotentialEnergyCorrection;
#endif
          p.KetjuIntegrated = false;
        }
    }
  TIMER_STOP(CPU_KETJU_UPDATE);
}

}  // namespace ketju
